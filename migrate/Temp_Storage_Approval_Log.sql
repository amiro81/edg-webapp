USE [dgweb]
GO

/****** Object:  Table [dbo].[Temp_Storage_Approval_Log]    Script Date: 29/3/2018 6:38:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Temp_Storage_Approval_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Document_No] [varchar](100) NULL,
	[UserId] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Remarks] [varchar](4000) NULL,
	[Approval_Level] [varchar](50) NULL,
	[Create_DateTime] [datetime] NULL,
 CONSTRAINT [PK_Approval_Log_Claim] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


