USE [dgweb]
GO

/****** Object:  Table [dbo].[Temp_Storage_Reg]    Script Date: 29/3/2018 6:38:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Temp_Storage_Reg](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[Reason] [varchar](4000) NULL,
	[Create_DateTime] [datetime] NULL,
	[Application_ID] [float] NULL,
	[Start_Datetime] [datetime] NULL,
	[End_Datetime] [datetime] NULL,
 CONSTRAINT [PK_Temp_Storage_Reg] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


