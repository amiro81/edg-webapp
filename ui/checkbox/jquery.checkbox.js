/*
 * jQuery checkbox
 * Original code from http://widowmaker.kiev.ua/checkbox/
 * 
 * @author Mohd Nawawi Mohamad Jamili
 */

(function($){
	$.fn.checkbox = function(settings) {
		settings = $.extend({ cls: 'jquery-checkbox', empty: 'checkbox-blank.png' }, settings || {} );

		/* IE6 background flicker fix */
		if ( $.browser.msie && $.browser.version <= 6 ) {
			try {
				document.execCommand('BackgroundImageCache', false, true);
			} catch (e) {};
		}
		
		/* remove event bubbling */
		function _cb(e) {
			e = e || window.event;
			e.cancelBubble = true;
			if (e.stopPropagation) {
				e.stopPropagation();
			}
		};

		/* Adds check/uncheck & disable/enable events */
		function addEvents(obj) {
			var checked = obj.checked, disabled = obj.disabled;
			
			if ( obj.stateInterval ) {
				clearInterval(obj.stateInterval);
			}
			
			obj.stateInterval = setInterval( function() {
				if ( obj.disabled != disabled ) {
					$(obj).trigger( (disabled = !!obj.disabled) ? 'disable' : 'enable');
				}
				if ( obj.checked != checked ) {
					$(obj).trigger( (checked = !!obj.checked) ? 'check' : 'uncheck');
				}
			},10);
			return $(obj);
		};
		
		/* Wrapping all passed elements */
		return this.each(function() {
			var ch, $_ch;
			ch = this; /* Reference to DOM Element*/
			$_ch = addEvents(ch); /* Adds custom events and returns, jQuery enclosed object */
			
			/* Removing wrapper if already applied  */
			if (ch.wrapper) ch.wrapper.remove();
			
			/* Creating wrapper for checkbox and assigning "hover" event */
			ch.wrapper = $('<span class="' + settings.cls + '"><span class="mark"><img src="' + settings.empty + '" /></span></span>');
			ch.wrapperInner = ch.wrapper.children('span:eq(0)');
			ch.wrapper.hover(
				function(e) { ch.wrapperInner.addClass(settings.cls + '-hover'); _cb(e); },
				function(e) { ch.wrapperInner.removeClass(settings.cls + '-hover'); _cb(e); }
			);
			
			/* Wrapping checkbox */
			$_ch.css({position: 'absolute', zIndex: -1, visibility: 'hidden'}).after(ch.wrapper);
			
			ch.wrapper.click(function(e) {
				$_ch.trigger('click',[e]);
				_cb(e);
				return false;
			});

			$_ch.click(function(e) {
				_cb(e);
			});

			$_ch.bind('disable', function() {
				ch.wrapperInner.addClass(settings.cls+'-disabled');
			}).bind('enable', function() {
				ch.wrapperInner.removeClass(settings.cls+'-disabled');
			});
			$_ch.bind('check', function() { 
				ch.wrapper.addClass(settings.cls+'-checked' );
			}).bind('uncheck', function() {
				ch.wrapper.removeClass(settings.cls+'-checked' );
			});
			
			/* Disable image drag-n-drop */
			$('img', ch.wrapper).bind('dragstart', function() {
				return false;
			}).bind('mousedown', function() {
				return false;
			});

			/* Firefox antiselection hack */
			if ( $.browser.mozilla && window.getSelection ) {
				ch.wrapper.css('MozUserSelect', 'none');
			}

			/* Applying checkbox state */
			if ( ch.checked ) {
				ch.wrapper.addClass(settings.cls + '-checked');
			}

			if ( ch.disabled ) {
				ch.wrapperInner.addClass(settings.cls + '-disabled');
			}
		});
	}
})(jQuery);
