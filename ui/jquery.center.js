/*
 * jQuery center plugin
 * http://www.gnu.org/licenses/gpl.html
 * Date: Fri 25 Jun 2010 01:42:55 PM MYT
 *
 * @author Mohd Nawawi Mohamad Jamili
 * @version 2
 */

(function($) {
	$.fn.center=function(settings) {
		settings=jQuery.extend({
			fixed: false,
			top: '50%',
			left: '50%',		
			resize: true,
			absolute: true
		}, settings);

		function _center(t) {
			var _zindex = parseInt(t.prev().css('z-index')) || 1;
			if ( _zindex < 0 ) _zindex = 1;
			_zindex++;
			t.css({
				position: settings.fixed ? 'fixed' : 'absolute',
				left: settings.left,
				top: settings.top, 
				zIndex: _zindex
			}).css({
				marginLeft: '-' + (t.outerWidth() / 2) + 'px', 
				marginTop: '-' + (t.outerHeight() / 2) + 'px'
			});

			if (settings.absolute) {
				t.css({
					marginTop: parseInt(t.css('marginTop'), 10) + jQuery(window).scrollTop(), 
					marginLeft: parseInt(t.css('marginLeft'), 10) + jQuery(window).scrollLeft()
				});
			}
		};

		return this.each(function() {
			var t=$(this);
			if ( settings.resize ) {
				$(window).bind("resize mouseenter", function() {
					_center(t);
				});
			};
			_center(t);
		});
	};
})(jQuery);
