if (typeof (_idx) !== 'undefined') {
	var _idx7 = _idx+"7";
	var _tpl7 = "content";
	var _action7 = "content";
	
	function _loadgrid7() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx7, { _req: 'tpl', _f: "content", tsec: "content", __t: time()  }, _tabfunc );
	};
	
	function _htmlbox(_pid,_id, _height, _width) {
		_width = _width || "600";
		_height = _height || "150";
		var _toobar = [["cut", "copy", "paste", "separator_dots", "bold", "italic", "underline", "strike", "sub", "sup", "separator_dots", "undo", "redo", "separator_dots",
								"left", "center", "right", "justify", "separator_dots", "ol", "ul", "indent", "outdent", "separator_dots", "link", "unlink", "image"]];
		var _titlebox = $(_pid+" #"+_id).css("height", _height).css("width", _width).htmlbox({
						toolbars: _toobar,
						about: false,
						icons: "default",
						skin: "default",
						idir: _baseurl+"/ui/htmlbox/images"
					});
		return _titlebox;
	};
	
	/* add */
	$("#"+_idx7+" button[name=btadd]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx7+'-add', _tpl = _tpl7+'-add', _formid = _idx7+'-save';
		var _width = 750;
		var _height = $(window).height() - 120;
		var _hbox_title = null, _hbox_text = null;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, _formid: _formid }).dialog({
				title: "Add New Content",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Save": function() {
						$(_pid+" .htmleditor").each(function() {
							var _val = $.trim($(this).attr("value"));
							_val = urlencode(_val);
							$(this).attr("value", _val );
						});

						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'save', _what: _action7 },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid7();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
					/*_hbox_title = _htmlbox(_pid,"title",30);*/
					_hbox_text = _htmlbox(_pid,"text",150);
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
					/*_hbox_title.remove();*/
					_hbox_text.remove();
					/*_hbox_title = null;*/
					_hbox_text = null;
				}
		});
	});
	
	/* edit */
	$("#"+_idx7+" img[data-edit]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr("data-edit");
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx7+'-edit', _tpl = _tpl7+'-edit', _formid = _idx7+'-update';
		var _width = 750;
		var _height = $(window).height() - 120;
		var _hbox_title = null, _hbox_text = null;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }).dialog({
				title: "Edit Content",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Update": function() {
						$(_pid+" .htmleditor").each(function() {
							var _val = $.trim($(this).attr("value"));
							_val = urlencode(_val);
							$(this).attr("value", _val );
						});
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'update', _what: _action7, id: _id },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid7();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
					/*_hbox_title = _htmlbox(_pid,"title",30);*/
					_hbox_text = _htmlbox(_pid,"text",150);
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
					/*_hbox_title.remove();*/
					_hbox_text.remove();
					/*_hbox_title = null;*/
					_hbox_text = null;
				}
		});
	});
	
	/* info */
	$("#"+_idx7+" div.grid-pinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx7+'-info', _tpl = _tpl7+'-info';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }).dialog({
				title: "Content Info",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Edit": function() {
						$("#"+_idx7+" img[data-edit="+_id+"]").trigger("click");
						window.setTimeout(function() {
							$(_pid).dialog("close");
						}, 1000);
					},
					"Close": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* delete */
	$("#"+_idx7+" img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _action7, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid7();
			},"text");
		});
	});
	
	$("#"+_idx7+" img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will remove "+_idlength+" data!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _action7, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid7();
				},"text");
			});
		}
	});
};