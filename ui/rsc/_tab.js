﻿if ( typeof (_cname) === 'undefined' ) {
	var _cname = 'cookietab';
}
if ( typeof (_tabindex) === 'undefined' ) {
	var _tabindex = $.readCookie(_cname);
	if ( _tabindex === null ) {
		_tabindex = 1;
	}
}
function _tabclean() {
	$("div.x-list").each(function () {
		$(this).empty();
	});
};
function _tabfunc(obj) {
	$(obj + " button[name=btadd], button.button_add").button({ icons: { primary: 'ui-icon-plus'} });
	$(obj + " button[name=btsearch]").button({ icons: { primary: 'ui-icon-search'} });
	$(obj + " button[name=btreloadgrid]").button({ icons: { primary: 'ui-icon-refresh'} });
	$(obj + " button[name=btreset]").button({ icons: { primary: 'ui-icon-refresh'} });
	$(obj + " button[name=btsubmit], button.button_check").button({ icons: { primary: 'ui-icon-check'} });
	$(obj + " button[name=btedit]").button({ icons: { primary: 'ui-icon-pencil'} });
	$(obj + " button[name=btcancel]").button({ icons: { primary: 'ui-icon-close'} });
	$(obj + " button.button_doc").button({ icons: { primary: 'ui-icon-document'} });
	$(obj + " input[type=checkbox][name=chkdel]").click(function () {
		var _checked = $(this).attr('checked') || false;
		if (_checked == false) {
			$(obj + ' input[type=checkbox][name^=del]').each(function () {
				$(this).attr('checked', true);
			});
		} else {
			$(obj + ' input[type=checkbox][name^=del]').each(function () {
				$(this).attr('checked', false);
			});
		}
	});
	var _hoverstat = function (obj) {
		var _file = $(obj).attr("src");
		_file = basename(_file);
		if (_file.match(/offd\.gif/) === null ) {
			if (_file.match(/on\.gif/)) {
				$(obj).attr({ 'src': _pbaseurl + '/ui/rsc/off.gif?' + time(), "data-tooltip":"Enable" });
			} else {
				$(obj).attr({ 'src': _pbaseurl + '/ui/rsc/on.gif?' + time(),"data-tooltip":"Disable" });
			}
		}
	};

	$(obj + " img[data-stat]").hover(
			function () { _hoverstat($(this)); },
			function () { _hoverstat($(this)); }
		);
	$(obj + " img[data-stat]").each(function() {
		var _var = $(this).attr("data-stat");
		var _opt = _var.split("|",2);
		if ( _opt && _opt[1]) {
			var _img = ( _opt[1] == 'True' ? 'on' : 'off' );
			$(this).attr({ 'src': _pbaseurl + '/ui/rsc/'+_img+'.gif?' + time() });
			if ( _img == "on" ) {
				$(this).attr("data-tooltip","Disable");
			} else {
				$(this).attr("data-tooltip","Enable");
			}
		}
	});
};
function _tabload(_id, index) {
	_tabclean();
	var $obj = $("div#sec > ul > li > a[href=#" + _id + "]");
	$obj.trigger("blur");
	if (index > 0) {
		$.setCookie(_cname, index, { duration: 1 });
		_tabindex = index;
	}
	var _code = $obj.attr("data-code") || null;
	if (_code != null) {
		if (_code.match(/^!p/)) {
			_redirect(_baseurl + '/#' + _code);
		} else {
			var _hash = location.hash || null;
			var p = _hash.match(/!p\/(\S+)\-t(\d+)\/(\S+)$/);
			var pp = _hash.match(/!p\/(\S+)\-(\S+)\/(\S+)$/);
			if (p && p[1] && p[2] && p[3]) {
				var arr = {};
				parse_str(p[3], arr);
				_page("div#sec > div#" + _id + " > div.x-list", array_merge({ _req: 'tpl', _f: _code }, arr), _tabfunc);
			} else if (pp && pp[1] && pp[2] && pp[3]) {
				var arr = {};
				parse_str(pp[3], arr);
				_page("div#sec > div#" + _id + " > div.x-list", array_merge({ _req: 'tpl', _f: _code }, arr), _tabfunc);
			} else {
				_page("div#sec > div#" + _id + " > div.x-list", { _req: 'tpl', _f: _code }, _tabfunc);
			}
		}
	}
};
$("#sec").tabs({
	cookie: {
		expires: 1
	},
	selected: (typeof (_sindex) !== 'undefined' ? _sindex : (typeof (_tabindex) !== 'undefined' ? _tabindex : 1)),
	select: function (e, ui) {
		e = e || window.event;
		if (ui.index == 0) {
			$.delCookie(_cname);
			$.hash.go('!p');
		}
	},
	show: function (e, ui) {
		var _id = ui.panel.id || null;
		if (_id !== null) {
			_tabload(_id, ui.index);
		}
	}
});

function _tabselect(index) {
	$("#sec").tabs("select", index);
};

/** eol **/