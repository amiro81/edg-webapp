/** tpl: tpl/_layout.cshtml **/

var _defaultpage = 'layout';

function _button_icon() {
	$(".button").button();
	$("button[name=btadd]").button({ icons: { primary: 'ui-icon-plus'} });
	$("button[name=btprofile]").button({ icons: {primary:'ui-icon-locked' }});
	$("button[name=btlogout]").button({ icons: {primary:'ui-icon-power' }});
	$("button[name=bthome]").button({ icons: {primary:'ui-icon-home' }});
	$("button[name=btreload], button[name=btreset]").button({ icons: {primary:'ui-icon-refresh' }});
	$("button[name=btlogin]").button({ icons: { primary: 'ui-icon-key'} });
	$("button[name=btsearch]").button({ icons: { primary: 'ui-icon-search'} });
	$("button[name=btadmin]").button({ icons: { primary: 'ui-icon-suitcase'} });
	$("button[name=btstatus]").button({ icons: { primary: 'ui-icon-notice'} });
	$("button[name=btpage]").button({ icons: { primary: 'ui-icon-clipboard'} });
};

function _hash_action(_hash) {
	_hash = _hash || null;
	if ( _hash ) {
		p = _hash.match(/!p\/(\S+)\-t(\d+)\/(\S+)$/);
		if ( p && p[1] && p[2] && p[3]) {
			var arr = {};
			parse_str(p[3], arr);
			_page("div.x-layout", array_merge({_req: 'tpl', _f: p[1], _tab: p[2] }, arr), _button_icon);
			return;
		}
		p = _hash.match(/!p\/(\S+)\-t(\d+)$/);
		if ( p && p[1] && p[2]) {
			_page("div.x-layout",{_req: 'tpl', _f: p[1], _tab: p[2] }, _button_icon);
			return;
		}
		p = _hash.match(/!p\/(\S+)\/(\S+)$/);
		if ( p && p[1] && p[2]) {
			var arr = {};
			parse_str(p[2], arr);
			_page("div.x-layout", array_merge({_req: 'tpl', _f: p[1] }, arr), _button_icon);
			return;
		}
		p = _hash.match(/!p\/(\S+)$/);
		if ( p && p[1]) {
			_page("div.x-layout",{_req: 'tpl', _f: p[1] }, _button_icon);
			return;
		}
		p = _hash.match(/!p$/);
		if ( p ) {
			_page("div.x-layout",{_req: 'tpl', _f: _defaultpage }, _button_icon );
			return;
		}
	}
	return false;
};

function _hash_init() {
	_button_icon();
	$.hash.init();
	$(document.body).hashchange(function (e, _hash) {
		_hash_action(_hash);
	});
};

$(window).load(function() {
	var _hash = location.hash || null;
	var _ok = false;
	if ( _hash === null ) {
		var _s = substr(window.location,-1);
		if ( _s === "/" ) {
			_ok = true;
			_redirect(_baseurl+'/#!p', true);
		}
	}
	if ( !_ok ) {
		if ( _hash_action(_hash) === false ) {
			_redirect(_baseurl+'/#!p', true);
			return;
		}
	}
});

$(document).ready(function () {
	_hash_init();

	$("button[name=btlogout], a[href^=#logout]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({
			async: false,
			cache: false, global: false,
			
		});
		var _ref = _baseurl+"/";
		$.get(_index, { _req: 'logout'}, function(data) {
	                _ajaxmsg("Logout");
			window.setTimeout(function() { _redirect(_ref); }, 500);
	        },"text");
	});

	$("div.bartop-logo, button[name=bthome]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.setCookie(_cname, 1, { duration: 1 });
		_redirect(_baseurl+'/#!p');
	});

	$("button[name=btreload]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		window.location.reload();
	});

	$("[data-page]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var page = $(this).attr("data-page") || null;
		var ti = $(this).attr("data-tab") || 1;
		if ( page !== null ) {
			$.setCookie(_cname, ti, { duration: 1 });
			$.hash.go("!p/"+page);
		}
	});
});

function _webgrid_link_hash(page,id,_fstr,_sstr,_sopt,_type,_sdate,_edate,_sort,_sortv,_sort2,_sortv2) {
	_type = _type || "";
	_sdate = _sdate || "";
	_edate = _edate || "";
	_sort = _sort || "";
	_sortv = _sortv || "";
	_sort2 = _sort2 || "";
	_sortv2 = _sortv2 || "";
	function _link(arg) {
		arg = arg.replace(/.*?\?/, "");
		if ( arg !== "" ) {
			var url = "!p/"+page+"/"+arg;
			if ( _fstr !== "" && _fstr !== null) {
				url += "&fstr="+_fstr;
			} else {
				if ( _sopt !== "" && _sopt !== null) {
					url += "&sopt="+_sopt;
				}
				if ( _sstr !== "" && _sstr !== null) {
					url += "&sstr="+_sstr;
				}
				if ( _type !== "" && _type !== null) {
					url += "&type="+_type;
				}
				if ( _sdate !== "" && _sdate !== null) {
					url += "&sdate="+_sdate;
				}
				if ( _edate !== "" && _edate !== null) {
					url += "&edate="+_edate;
				}
				if ( _sort !== "" && _sort !== null && _sortv !== "" && _sortv !== null) {
					url += "&"+_sort+"="+_sortv;
				}
				if ( _sort2 !== "" && _sort2 !== null && _sortv2 !== "" && _sortv2 !== null) {
					url += "&"+_sort2+"="+_sortv2;
				}
			}
			url +="&__t="+time();
			$.hash.go(url);
		}
	};
	$("#"+id+" thead th a").click(function(e) {
		e = e || window.event;
		e.preventDefault();
		var t = $(this).attr("href");
		_link(t);
	});
	$("#"+id+" tfoot td a").click(function(e) {
		e = e || window.event;
		e.preventDefault();
		var t = $(this).attr("href");
		t = t.replace(/.*?\?/, "");
		_link(t);
	});
	$("span.grid-footer a").click(function(e) {
		e = e || window.event;
		e.preventDefault();
		var t = $(this).attr("href");
		t = t.replace(/.*?\?/, "");
		_link(t);
	});
};