if (typeof (_idx) !== 'undefined') {
	var _idx5 = _idx+"5";
	var _tpl5 = "smslog";
	var _action5 = "smslog";
	
	function _loadgrid_smslog() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx5, { _req: 'tpl', _f: "smslog", tsec: "smslog", __t: time() }, _tabfunc );
	};
	
	$("#"+_idx5+" button[name=btexcel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		_popupnewin(_baseurl+'/?_req=smslogexport','new');
	});
	
	$("#"+_idx5+" button[name=btcancel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to remove SMS Log?", function() {
			$.post(_index, {  _post: 'clearall', _what: _action5 }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid_smslog();
			},"text");
		});
	});
	
	/* delete */
	$("#"+_idx5+" img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _action5, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid_smslog();
			},"text");
		});
	});
	
	$("#"+_idx5+" img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will remove "+_idlength+" data!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _action5, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid_smslog();
				},"text");
			});
		}
	});

	/* info */	
	$("#"+_idx5+" div.grid-pinfo[data-appid]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _appid = $(this).attr("data-appid");
		if ( _appid !== "" ) {
			_redirect(_baseurl+"/#!p/status-t1/sopt=Application_ID&sstr=" + urlencode(_appid) + "&tsec=status-current&__t="+time() );
		}
	});
	$("#"+_idx5+" div.grid-pinfo[data-mobile]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _mobile = $(this).attr("data-mobile");
		if ( _mobile !== "" ) {
			_redirect(_baseurl+"/#!p/admin-t5/sopt=Mobile&sstr=" + urlencode(_mobile) + "&tsec=sms&__t="+time() );
		}
	});
};