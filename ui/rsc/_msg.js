if (typeof (_idx) !== 'undefined') {
	var _idx4 = _idx+"4";
	var _tpl4 = "msg";
	var _action4 = "msg";
	
	function _loadgridm() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx4, { _req: 'tpl', _f: "msg", tsec: "msg", __t: time()  }, _tabfunc );
	};

	/* info */	
	function _pinfo(_id) {
		var _pid = '#'+_idx4+'-chat', _tpl = _tpl4+'-chat';
		$(_pid).empty().dialog("destroy");
		var _width = 700;
		var _height = $(window).height() - 250;
					var _applicationid = "";
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }, function() {
			$(_pid+" > div.dialog_max_height > div.msglist").load(_index, { _req: 'tpl', _f: _tpl+"-chat", id: _id }, function() {
				_applicationid = $(_pid+" > div.dialog_max_height > div.msglist input[name=appid]").attr("value");
			});
		
		}).dialog({
				title: "Message for Application Id "+_applicationid,
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", 30],
				buttons: {
					"Submit": function() {
						var _text = $.trim($(this).parent().find('.ui-dialog-buttonpane textarea[name=text]').val());
						var _appid = $(_pid+" > div.dialog_max_height > div.msglist input[name=appid]").attr("value");
						var _mid = $(_pid+" > div.dialog_max_height > div.msglist input[name=mid]").attr("value");
						var _uid = $(_pid+" > div.dialog_max_height > div.msglist input[name=uid]").attr("value");
						if ( _text === "" || _appid === "" || _mid === "" || _uid === "") {
							return false;
						}
						var _post = { 
							_post: 'save', _what: 'msg',
							Application_ID: _appid,
							Master_ID: _mid,
							Text: _text,
							UserId: _uid
						};

						$.post(_index, _post, function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								/*_loadgridm();*/
								$(_pid+" > div.dialog_max_height > div.msglist").load(_index, { _req: 'tpl', _f: _tpl+"-chat", id: _id });
								/*_pinfo(_id);*/
						},"text");
					},
					"Close": function() {
						$(this).dialog("close");
						_loadgridm();
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					$(this).parent().find('.ui-dialog-buttonpane button:contains("Submit")').button({
						icons: { primary: 'ui-icon-circle-check' }
					});
					$(this).parent().find('.ui-dialog-buttonpane').prepend("<textarea class='text' style='width:100%;height:50px;' name='text' placeholder='Type message..'></textarea>");
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	};
	$("#"+_idx4+" div.grid-pinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({async: false, global: false, cache: true});
		_pinfo(_id);
	});
};