if (typeof (_datastatus) !== 'undefined') {

	$("span[data-status]").each(function() {
		var _id = $(this).attr("data-id");
		var _dt = $(this).attr("data-status");
		var _ds = _dt.split(",");
		var _cl = "#FFFFFF";
		if ( _ds[0] == '0' && _ds[1] == '1' ) {
			_cl = "#FFFFCC";
		} else if ( _ds[0] == '1' && _ds[1] == '1' ) {
			_cl = "#BBFFBB";
		} else if ( _ds[0] == '2' || _ds[1] == '2' ) {
			_cl = "#FFBFBF";
		} else if ( _ds[0] == '3' || _ds[1] == '3' ) {
			_cl = "#F5701D";			
		}
		$(this).parent().parent().css("background", _cl).hover(
			function() {
				/*$(this).css("background","#C7FC8D");*/
				if ( _cl == "#FFFFFF" ) {
					$(this).css("background","#eeeeee");
				}
				$(this).css("cursor","pointer");
			},
			function() {
				$(this).css({"background":_cl,"cursor":"default"});
			}
		);
		/*$(this).parent().parent().click(function(e) {
			e = e || window.event;
			e.preventDefault();
			_redirect(_baseurl+'/#!p/status-details/id='+_id+'&ref='+_datastatus,true);
		});*/
	});
	
	/*$("span.grid-sinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		_redirect(_baseurl+'/#!p/status-details/id='+_id+'&ref='+_datastatus,true);
	});*/
	$("span.grid-sinfo").each(function() {
		var _tt = $(this);
		$(this).parent().click(function(e) {
			e.preventDefault();
			var _id = _tt.attr('data-id') || null;
			_redirect(_baseurl+'/#!p/status-details/id='+_id+'&ref='+_datastatus,true);
		});
	});
	
	$("div.grid-search .datepicker").datepicker({
		dateFormat: 'dd-mm-yy',
		changeYear: true,
		changeMonth: true,
		yearRange: '2000:c+5',
		onChangeMonthYear: function (year, month, inst) {
			var dd = (inst.currentDay < 10 ? "0" + inst.currentDay : inst.currentDay);
			if (dd == "00") {
				dd = "01";
			}
			var mm = (month < 10 ? "0" + month : month);
			var ct = dd + '-' + mm + '-' + year;
			$(this).attr("value", ct);
		}
	});

};