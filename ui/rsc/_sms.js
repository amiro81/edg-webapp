if (typeof (_idx) !== 'undefined') {
	var _idx5 = _idx+"5";
	var _tpl5 = "sms";
	var _action5 = "user";
	
	function _loadgrid5() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx5, { _req: 'tpl', _f: "sms", tsec: "sms", __t: time() }, _tabfunc );
	};
	
	/* add */
	/*$("#"+_idx5+" button[name=btadd]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({async: false, global: false});
		var _pid = '#'+_idx5+'-add', _tpl = _tpl5+'-add', _formid = _idx5+'-save';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, _formid: _formid }, function() {
			$(_pid+" select[name=Category]").change(function() {
				var _val = $(this).attr("value");
				if ( _val == "KPA" ) {
					$(_pid+" tr#isadmin").show();
				} else {
					$(_pid+" tr#isadmin").hide();
				}
			});
			$(_pid+" select[name=Category]").trigger("change");
		}).dialog({
				title: "Add New User",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Save": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'save', _what: _action5 },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid5();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});*/
	
	/* edit */
	$("#"+_idx5+" img[data-edit]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr("data-edit");
		$.ajaxSetup({async: false, global: false});
		var _pid = '#'+_idx5+'-edit', _tpl = _tpl5+'-edit', _formid = _idx5+'-update';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }, function() {
			$(_pid+" select[name=Category]").change(function() {
				var _val = $(this).attr("value");
				if ( _val == "KPA" ) {
					$(_pid+" tr#isadmin").show();
				} else {
					$(_pid+" tr#isadmin").hide();
				}
			});
			$(_pid+" select[name=Category]").trigger("change");
		}).dialog({
				title: "Edit SMS",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Update": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'updatesmsno', _what: _action5, id: _id },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid5();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* info */
	$("#"+_idx5+" div.grid-pinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({async: false, global: false});
		var _pid = '#'+_idx5+'-info', _tpl = _tpl5+'-info';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }).dialog({
				title: "SMS Info",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Edit": function() {
						$("#"+_idx5+" img[data-edit="+_id+"]").trigger("click");
						window.setTimeout(function() {
							$(_pid).dialog("close");
						}, 1000);
					},
					"Close": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* delete */
	/*$("#"+_idx5+" img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _action5, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid5();
			},"text");
		});
	});
	
	$("#"+_idx5+" img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will remove "+_idlength+" data!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _action5, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
					}
					_loadgrid5();
				},"text");
			});
		}
	});*/
	
	/* SMS status */
	$("#"+_idx5+" img[data-stat]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _obj = $(this);
		var _var = $(this).attr("data-stat");
		var _opt = _var.split("|",2);
		if ( _opt ) {
			var _status = ( _opt[1] == 'True' ? '0' : '1' );
			$.ajaxSetup({
				async: false,
				cache: false, global: false
			});
			$.post(_index, { _post: 'updatesms', _what: _action5, id: _opt[0], Sms: _status }, function(data) {
				if ( !_ismsg_json(data) ) {
				        _gerror(data);
				        return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
				        _gid = _gfalse(data.msg);
						_ghoverclose(_gid);
				        /*return false;*/
				}
				_loadgrid5();
			},"text");
		}
	});
	
	$("#"+_idx5+" img[data-click=menable], #"+_idx5+" img[data-click=mdisable]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _xx = $(this).attr("data-click");
		var _text = { "menable": "This operation will enable", "mdisable": "This operation will disable"  };
		var _stat = { "menable": "1", "mdisable": "0" };

		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_confirm(_text[_xx]+" "+_idlength+" data", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'updatesms', Sms: _stat[_xx], _what: _action5, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid5();
				},"text");
			});
		}
	});
};