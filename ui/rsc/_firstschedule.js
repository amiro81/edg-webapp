if (typeof (_idxfs) !== 'undefined') {
	var _idxfs = _idxfs+"fs";
	var _tplfs = "firstschedule";
	var _actionfs = "firstschedule";
	
	function _loadgrid_fs() {
		$.hash.go("!p/firstschedule");
	};
	
	/* add */
	$("#firstschedule button[name=btadd]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idxfs+'-add', _tpl = _tplfs+'-add', _formid = _idxfs+'-save';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, _formid: _formid }).dialog({
				title: "Add New Data",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Save": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'save', _what: _actionfs },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid_fs();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* edit */
	$("div.barmid-fschedule-grid img[data-edit]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr("data-edit");
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idxfs+'-edit', _tpl = _tplfs+'-edit', _formid = _idxfs+'-update';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }).dialog({
				title: "Edit Data",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Update": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'update', _what: _actionfs, id: _id },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);_loadgrid_fs();
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid_fs();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* info */
	$("div.barmid-fschedule-grid span.grid-info").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idxfs+'-info', _tpl = _tplfs+'-info';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }).dialog({
				title: "Firstschedule Info",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Edit": function() {
						$("div.barmid-fschedule-grid img[data-edit="+_id+"]").trigger("click");
						window.setTimeout(function() {
							$(_pid).dialog("close");
						}, 1000);
					},
					"Close": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* delete */
	$("div.barmid-fschedule-grid img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _actionfs, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid_fs();
			},"text");
		});
	});
	
	$("div.barmid-fschedule-grid img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will remove "+_idlength+" data!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _actionfs, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid_fs();
				},"text");
			});
		}
	});
};