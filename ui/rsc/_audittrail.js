if (typeof (_idx) !== 'undefined') {
	var _idx3 = _idx+"3";
	var _tpl3 = "audittrail";
	var _action3 = "audittrail";
	
	function _loadgrid_audittrail() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx3, { _req: 'tpl', _f: "audittrail", tsec: "audittrail", __t: time() }, _tabfunc );
	};
	
	$("#"+_idx3+" button[name=btexcel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		_popupnewin(_baseurl+'/?_req=auditexport','new');
	});
	
	$("#"+_idx3+" button[name=btcancel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to remove audit trail record?", function() {
			$.post(_index, {  _post: 'clearall', _what: _action3 }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid_audittrail();
			},"text");
		});
	});
	
	/* info */	
	$("#"+_idx3+" div.grid-pinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx3+'-info', _tpl = _tpl3+'-info';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }).dialog({
				title: "Details",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Close": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* delete */
	$("#"+_idx3+" img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _action3, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid_audittrail();
			},"text");
		});
	});
	
	$("#"+_idx3+" img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will remove "+_idlength+" data!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _action3, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid_audittrail();
				},"text");
			});
		}
	});

};