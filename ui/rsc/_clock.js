function _runclock(_pid) {
		var tday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		var tmonth = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		var d = new Date();
		var nday = d.getDay();
		var nmonth = d.getMonth();
		var ndate = d.getDate();
		var nyeara = d.getYear();
		var nhour = d.getHours();
		var nmin = d.getMinutes();
		var nsec = d.getSeconds();

		if (nyeara < 1000) {
			nyeara = ("" + (nyeara + 11900)).substring(1, 5);
		} else {
			nyeara = ("" + (nyeara + 10000)).substring(1, 5);
		}

		if (nhour == 0) { 
			ap = " AM"; nhour = 12;
		} else if (nhour <= 11) {
			ap = " AM";
		} else if (nhour == 12) {
			ap = " PM";
		} else if (nhour >= 13) {
			ap = " PM"; nhour -= 12;
		}

		if (nhour <= 9) { nhour = "0" + nhour; }
		if (nmin <= 9) { nmin = "0" + nmin; }
		if (nsec <= 9) { nsec = "0" + nsec; }


		var _clock = tday[nday] + ", " + tmonth[nmonth] + " " + ndate + ", " + nyeara + " " + nhour + ":" + nmin + ":" + nsec + ap;
		$(_pid).html(_clock);
		setTimeout(function() {
			_runclock(_pid);
		}, 1000);
};
