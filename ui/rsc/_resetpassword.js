function _resetpassword(_id) {
	$.ajaxSetup({ async: false, global: false, cache: true });
	var _pid = 'div#_bpx', _tpl = 'changepassword', _formid = '_bpx-user-changepasswd';
	var _height = $(window).height() - 120;
	$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }).dialog({
		modal: true,
		width: 600,
		resizable: true,
		position: ["center", "center"],
		buttons: {
			"Update": function () {
				$('#' + _formid).ajaxSubmit({
					url: _index,
					type: 'POST',
					method: 'POST',
					dataType: 'text',
					async: false,
					clearForm: false,
					resetForm: false,
					cache: false,
					data: { _post: 'changepassword', _what: 'user', id: _id },
					success: function (data) {
						if (!_ismsg_json(data)) {
							_gerror(data);
							return false;
						}
						data = $.evalJSON(data);
						if (!data.success) {
							_gfalse(data.msg);
							return false;
						}
						_gid = _gtrue(data.msg);
						_ghoverclose(_gid);
						var _ref = _baseurl+"/";
						$.get(_index, { _req: 'logout'}, function(data) {
							_ajaxmsg("Logout");
							window.setTimeout(function() { _redirect(_ref); }, 500);
						},"text");
					}
				});
				return false;
			}
		},
		open: function () {
			_dialog_maxheight($(this),_height);
			$(".ui-dialog-titlebar-close").hide();
			_buttondialog($(this));
		},
		close: function () {
			$(_pid).empty().dialog("destroy");
		}
	});
};