/*
 * behavior script
 *
 * @author Mohd Nawawi Mohamad Jamili <nawawi@rutweb.com>
 * @category javascript
 * @update 19-Oct-2011
 */

/**
 * History:
 *
 * 25/07/2012 - fork for .net webpages C# razor
 */

if ( typeof(_baseurl) == 'undefined' ) {
        var _baseurl = "";
}
if ( typeof(_pbaseurl) == 'undefined' ) {
        var _pbaseurl = _baseurl;
}

var _uiurl = _pbaseurl + '/ui';
var _rscurl = _uiurl + '/rsc';

/* http://stackoverflow.com/questions/7825448/webkit-issues-with-event-layerx-and-event-layery */
$.event.props = $.event.props.join('|').replace('layerX|layerY|', '').split('|');

function _checkbox_fix() {
	if ( $.isFunction($().checkbox) ) {
		$('input[type=checkbox]').not("input[name^=multiselect]").checkbox({'empty': _uiurl+'/checkbox/checkbox-blank.png'});
	}
};

function _xtooltip() {
	if ( $.isFunction($().xtooltip) ) {
		$(document.body).bind('mouseover.tooltip',function() {
			$(this).find('[data-tooltip]').each(function() {
				$(this).xtooltip();
			});
		}).bind("unload", function() {
			$(this).unbind("mouseover.tooltip");
		});
	}
};

function _button() {
	if ( $.isFunction($().button) ) {
		$(".button").button();
	}
};

function _placeholder() {
	if ( $.isFunction($().placeholder) ) {
		$('input[placeholder], textarea[placeholder]').not(".placeholder").placeholder();
	}
};

function _fixjs() {
	_xtooltip();
	_checkbox_fix();
	_button();
	_placeholder();
};

$(document).ready(function() {
	_fixjs();
});

function _ajaxloader(show) {
	show = show || false;
	$("#ajax-loader").hide();
	if ( show ) {
		$("#ajax-loader").center().show();
	}
};

if ( $.isPlainObject($.gritter) ) {
	$.extend($.gritter.options, { 
		fade_in_speed: 100,
		fade_out_speed: 100,
		time: 4000
	});
};

function _gmsg(settings) {
	if ( $.isPlainObject($.gritter) ) {
		settings = $.extend({
			sticky: false
		}, settings);
		var _id = $.gritter.add(settings);
		$("#gritter-notice-wrapper").center({'top':'35%'}).zindex();
		return _id;
	}
	alert(settings.title+' '+settings.text);
	return null;
};

function _ghoverclose(id) {
	if ( $.isPlainObject($.gritter) ) {
		$("#gritter-notice-wrapper").mouseover(function() {
			$.gritter.remove(id,{speed: 'fast'});
		});
	}
};

function _gmsgremove() {
	$("#gritter-notice-wrapper").remove();
};

function _gwarning(msg, title) {
	title = title || "Warning";
	msg = msg || ' ';
	var _id = _gmsg({
		title: title,
		text: msg,
		sticky: false,
		image: _uiurl+'/gritter/gritter-warning.png'
	});
	return _id;
};

function _gnotice(msg, title) {
	title = title || "Notice";
	msg = msg || ' ';
	var _id = _gmsg({
		title: title,
		text: msg,
		sticky: false,
		image: _uiurl+'/gritter/gritter-notice.png'
	});
	return _id;
};

function _gerror(msg, title) {
	title = title || "Error";
	msg = msg || ' ';
	var _id = _gmsg({
		title: title,
		text: msg,
		sticky: false,
		image: _uiurl+'/gritter/gritter-error.png'
	});
	return _id;
};

function _gtrue(msg, title) {
	title = title || ' ';
	msg = msg || ' ';
	var _id = _gmsg({
		title: title,
		text: msg,
		sticky: false,
		image: _uiurl+'/gritter/gritter-true.png'
	});
	return _id;
};

function _gfalse(msg, title) {
	title = title || ' ';
	msg = msg || ' ';
	var _id = _gmsg({
		title: title,
		text: msg,
		sticky: true,
		image: _uiurl+'/gritter/gritter-false.png'
	});
	_ghoverclose(_id);
	return _id;
};

function _ismsg_json(data) {
	return ( /^{/.test(data) );
};

/* ajax loader */
$(document).ready(function() {
	$("div#ajax-loader").center({top: "1%"});
});
var _ajaxloader_timer = null;
function _ajaxloader(show, title) {
	title = title || "Loading..";
	show = show || false;
	if ( _ajaxloader_timer !== null ) {
		window.clearTimeout(_ajaxloader_timer);
		_ajaxloader_timer = null;
	}
	$("#ajax-loader").center({top: "1%", absolute: false, fixed: true}).zindex().html(title);
	if ( show ) {
		$("#ajax-loader").show();
	} else {
		_ajaxloader_timer = window.setTimeout(function() {
			$("#ajax-loader").css("z-index","1").hide().empty();
		}, 900);
	}
};

var _ajaxmsg_timer = null;
function _ajaxmsg(msg, timeout) {
	timeout = timeout || 1000;
	if ( _ajaxmsg_timer !== null ) {
		window.clearTimeout(_ajaxmsg_timer);
		_ajaxmsg_timer = null;
	}
	$("#ajax-msg").center({top: "1%"}).zindex().html(msg).show();
	_ajaxmsg_timer = window.setTimeout(function() {
			$("#ajax-msg").css("z-index","1").hide().empty();
		}, 1000);
};

function _debugtrace(data) {
	var debugwin = open("","debugtrace_win"+time());
	debugwin.document.write(data);
	debugwin.focus();
	delete data;
};

$.ajaxSetup({
	global: true,
	async: true,
	cache: false,
	headers: { "X-AJAX-REQUEST": "OK" },
	beforeSend: function() {
		if ( !$.support.placeholder ) {
			$("input[placeholder], textarea[placeholder]").each(function() {
				var _text = $.trim($(this).attr("value")) || null, _ptext = $(this).attr("placeholder") || null;
				if ( _text != null && _ptext != null ) {
					if ( _text == _ptext ) {
						$(this).attr("value", "");
					}
				}
				
			});
		}
	},
	success: function() {
		_fixjs();
	},
	complete: function() {
		_fixjs();
	},
	error: function(e, s, t) {
		_fixjs();
		_ajaxloader(false);
		var msg="";
		if ( s !== null ) {
			msg += "Status: "+s+"<br>";
		}

		if ( !$.isEmptyObject(t) ) {
			msg += "Error: "+print_r(t,true)+"<br>";
		}
		if ( !$.isEmptyObject(e) ) {
			if ( e.responseText ) {
				_debugtrace(e.responseText);
			}
		}
		_gerror(msg);
	}
});

function _redirect(_url, _replace) {
	_replace = _replace || false;
	if ( _replace ) {
		self.location.replace(_url);
		return;
	}
	self.location.href = _url;
};

function _page(obj,arg, func) {
    func = func || null;
	$.ajaxSetup({
			global: false,
			async: true,
			cache: false,
			beforeSend: function() {
				_ajaxloader(true);
			},
			success: function() {
				_ajaxloader(false);
			},
			complete: function() {
				_ajaxloader(false);
			}
		});

	$(obj).load(_index, arg, function() {
		if ( func !== null ) {
			func(obj);
		}
		$(this).show();
		_fixjs();
	});
};

/* win popup */
function _popup(w,h,u) {
	var pop = window.open(u,'popup','width='+w+',height='+h+',scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');
	if ( !pop ) {
		_gerror("Please allow window popup!");
	}
	if ( !pop ) {
		_gerror("Please allow window popup!");
		return false;
	}
	pop.focus();
	return true;
};
function _popupfull(u,t) {
	t = t || 'site';
	var w = $(window).width();
	var h = $(window).height();
	var pop = window.open(u,t,'width='+w+',height='+h+',fullscreen=yes,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');
	if ( !pop ) {
		_gerror("Please allow window popup!");
		return false;
	}
	pop.focus();
	return true;
};

function _popupnewin(u,t) {
	t = t || 'new';
	var pop = window.open(u,t);
	if ( !pop ) {
		_gerror("Please allow window popup!");
		return false;
	}
	pop.focus();
	return true;
};

function _winreload() {
	window.location.reload();
};

/*function _removeckeditor() {
	if ( typeof(CKEDITOR) != 'undefined' && CKEDITOR.instances) {
		for (var name in CKEDITOR.instances) {
			CKEDITOR.instances[name].destroy(true);
			delete CKEDITOR.instances[name];
		}
	}
};*/

/* workaround for maxHeight issue: http://bugs.jqueryui.com/ticket/4820 */
function _dialog_maxheight(obj,height) {
	obj.find('.dialog_max_height').css('max-height',height+'px');
};

/* default dialog button icon */
function _buttondialog(obj) {
	obj.parent().find('.ui-dialog-buttonpane button:contains("Cancel")').button({
		icons: { primary: 'ui-icon-circle-close' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("Close")').button({
		icons: { primary: 'ui-icon-closethick' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("Update")').button({
		icons: { primary: 'ui-icon-circle-check' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("Save")').button({
		icons: { primary: 'ui-icon-disk' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("Edit")').button({
		icons: { primary: 'ui-icon-pencil' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("Yes")').button({
		icons: { primary: 'ui-icon-circle-check' }
	});
	obj.parent().find('.ui-dialog-buttonpane button:contains("No")').button({
		icons: { primary: 'ui-icon-circle-close' }
	});
};

function _confirm(msg,func_yes,func_no,_title,_pid,_width,_height) {
	_pid = _pid || "#dialog";
	func_yes = func_yes || null;
	func_no = func_no || null;
	_title = _title || "Confirmation";
	_width = _width || 300;
	_height = _height || null;
	var _height = $(window).height() - 20;
	var $pid = $(_pid);

	$pid.html(msg).dialog({
		modal: true,
		stack: true,
		position: ["center", 150],
		title: _title,
		width: _width,
		buttons: {
			"No": function() {
				if ( func_no !== null ) {
					func_no();
				}
				$pid.dialog("close");
			},
			"Yes": function() {
				if ( func_yes !== null ) {
					func_yes();
				}
				$pid.dialog("close");
			}
		},
		open: function() {
			var $this = $(this);
			_dialog_maxheight($this,_height);
			_buttondialog($this);
		},
		close: function() {
			$pid.empty().dialog("destroy");
		}
	});
};

/* input next focus */
$.extend($.expr[':'], {
	focusable: function(element) {
			var nodeName = element.nodeName.toLowerCase(), tabIndex = $.attr(element, 'tabindex');
			return (/input|select|textarea|button|object/.test(nodeName) ? !element.disabled : 'a' == nodeName || 'area' == nodeName ? element.href || !isNaN(tabIndex) : !isNaN(tabIndex)) && !$(element)['area' == nodeName ? 'parents' : 'closest'](':hidden').length;
		}
});

$("input[type=text], input[type=password]").live("keydown",function(e) {
	e = e || window.event;
	var knum = document.all ? e.keyCode : e.which;
	var $this = $(this), $focusables = $(':focusable'), current = $focusables.index(this), $next;
	if (knum == '13') {
		$next = $focusables.eq(current+1).length ?$focusables.eq(current+1) : $focusables.eq(0);
		$next.focus();
	}
});

function isNumberKey(evt,decimal) {
	decimal = decimal || false;
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if ( decimal) {
		if ( charCode == 46 ) {
			return true;
		}
	}
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
};

/* lpktn edg */
$(document).ready(function () {
	function _bghack() {
		var img = $("body").css("background-image");
		if ($(document).width() <= 1024) {
			if (img.match(/bg\.jpg/)) {
				$("body").css("background-image", "url(" + _rscurl + "/bg1024a.jpg)");
			}
		} else {
			if (img.match(/bg1024a\.jpg/)) {
				$("body").css("background-image", "url(" + _rscurl + "/bg.jpg)");
			}
		}
	};
	$(window).bind("resize", _bghack);
	_bghack();
});

