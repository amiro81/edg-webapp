if (typeof (_idx) !== 'undefined') {
	var _idx1 = _idx+"1";
	var _tpl1 = "user";
	var _action = "user";
	
	function _loadgrid1() {
		var _hash = location.hash || null;
		if ( _hash !== null ) {
			if ( _hash.match(/\#\!p\/\S+\/\S+/) ) {
				_hash = _hash.replace(/^#/,'').replace(/&__t=.*/,'');
				_hash = _hash+'&__t='+time();
				$.hash.go(_hash);
				return;
			}
		}
		_page("#"+_idx1, { _req: 'tpl', _f: "user", tsec: "user", __t: time()  }, _tabfunc );
	};
	
	/* add */
	$("#"+_idx1+" button[name=btadd]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx1+'-add', _tpl = _tpl1+'-add', _formid = _idx1+'-save';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, _formid: _formid }, function() {
			$(_pid+" select[name=Category]").change(function() {
				var _val = $(this).attr("value");
				if ( _val == "KPA" ) {
					$(_pid+" tr#isadmin").show();
				} else {
					$(_pid+" tr#isadmin").hide();
				}
				if ( _val == "KPC" ) {
					$(_pid+" tr#isview").show();
				} else {
					$(_pid+" tr#isview").hide();
				}
			});
			$(_pid+" select[name=Category]").trigger("change");
		}).dialog({
				title: "Register As New User",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Save": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'save', _what: _action },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid1();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* edit */
	$("#"+_idx1+" img[data-edit]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr("data-edit");
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx1+'-edit', _tpl = _tpl1+'-edit', _formid = _idx1+'-update';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }, function() {
			$(_pid+" select[name=Category]").change(function() {
				var _val = $(this).attr("value");
				if ( _val == "KPA" ) {
					$(_pid+" tr#isadmin").show();
				} else {
					$(_pid+" tr#isadmin").hide();
				}
				if ( _val == "KPC" ) {
					$(_pid+" tr#isview").show();
				} else {
					$(_pid+" tr#isview").hide();
				}
			});
			$(_pid+" select[name=Category]").trigger("change");
		}).dialog({
				title: "Edit User",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Update": function() {
						$('#'+_formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'update', _what: _action, id: _id },
							success: function(data) {
								if ( !_ismsg_json(data) ) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if ( !data.success ) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								_loadgrid1();
								window.setTimeout(function() {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* info */
	$("#"+_idx1+" div.grid-pinfo").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-id') || null;
		$.ajaxSetup({ async: false, global: false, cache: true });
		var _pid = '#'+_idx1+'-info', _tpl = _tpl1+'-info';
		var _width = 600;
		var _height = $(window).height() - 120;
		$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id }).dialog({
				title: "User Info",
				modal: true,
				width: _width,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Edit": function() {
						$("#"+_idx1+" img[data-edit="+_id+"]").trigger("click");
						window.setTimeout(function() {
							$(_pid).dialog("close");
						}, 1000);
					},
					"Reset Password": function() {
						$("#"+_idx1+" img[data-pwdreset="+_id+"]").trigger("click");
					},
					"Close": function() {
						$(this).dialog("close");
					}
				},
				open: function() {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
					$(this).parent().find('.ui-dialog-buttonpane button:contains("Reset Password")').button({
						icons: { primary: 'ui-icon-arrowrefresh-1-s' }
					});
				},
				close: function() {
					$(_pid).empty().dialog("destroy");
				}
		});
	});
	
	/* delete */
	$("#"+_idx1+" img[data-del]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-del');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("By delete this user will delete any application associated with it. Are you sure you want to delete?", function() {
			$.post(_index, {  _post: 'delete', _what: _action, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_loadgrid1();
			},"text");
		});
	});
	
	$("#"+_idx1+" img[data-click=mdel]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will delete "+_idlength+" of user(s). By delete this user(s) will delete any application associated with it. Are you sure you want to continue?", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'delete', _what: _action, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid1();
				},"text");
			});
		}
	});
	
	/* status */
	$("#"+_idx1+" img[data-stat]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _obj = $(this);
		var _var = $(this).attr("data-stat");
		var _opt = _var.split("|",2);
		if ( _opt ) {
			var _status = ( _opt[1] == 'True' ? '0' : '1' );
			$.ajaxSetup({
				async: false,
				cache: false, global: false
			});
			$.post(_index, { _post: 'updatestatus', _what: _action, id: _opt[0], status: _status }, function(data) {
				if ( !_ismsg_json(data) ) {
				        _gerror(data);
				        return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
				        _gid = _gfalse(data.msg);
						_ghoverclose(_gid);
				        /*return false;*/
				}
				_loadgrid1();
			},"text");
		}
	});
	
	$("#"+_idx1+" img[data-click=menable], #"+_idx1+" img[data-click=mdisable]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _xx = $(this).attr("data-click");
		var _text = { "menable": "This operation will enable", "mdisable": "This operation will disable"  };
		var _stat = { "menable": "1", "mdisable": "0" };

		/*var _id = new Array();*/
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			/*_id.push(_p[1]);*/
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		/*if ( _id.length > 0 ) {*/
		if ( _idlength > 0 ) {
			_confirm(_text[_xx]+" "+_idlength+" data", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'updatestatus', status: _stat[_xx], _what: _action, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
						/*return false;*/
					}
					_loadgrid1();
				},"text");
			});
		}
	});
	
	/* reset password */
	$("#"+_idx1+" img[data-pwdreset]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = $(this).attr('data-pwdreset');
		$.ajaxSetup({
			async: false,
			cache: false, global: false
		});
		_confirm("Are you sure you want to reset this user password?", function() {
			$.post(_index, {  _post: 'resetpassword', _what: _action, id: _id }, function(data) {
				if ( !_ismsg_json(data) ) {
					_gerror(data);
					return false;
				}
				data = $.evalJSON(data);
				if ( !data.success ) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					return false;
				}
				_gtrue(data.msg);
				_loadgrid1();
			},"text");
		});
	});
	$("#"+_idx1+" img[data-click=mpwdreset]").live("click", function(e) {
		e = e || window.event;
		e.preventDefault();
		var _id = "", _idlength = 0;
		$('input[type=checkbox][name^=del]:checked').each(function() {
			var _name = $(this).attr("name");
			var _p = _name.match(/del\[(\d+)\]/);
			if ( _p && _p[1] ) {
				_id += _p[1] +",";
				_idlength++;
			}
		});
		if ( _idlength > 0 ) {
			_id = rtrim(_id,",");
			_confirm("This operation will reset "+_idlength+" password!", function() {
				$.ajaxSetup({
					async: false,
					cache: false, global: false
				});
				$.post(_index, { _post: 'resetpassword', _what: _action, id: _id}, function(data) {
					if ( !_ismsg_json(data) ) {
						_gerror(data);
						return false;
					}
					data = $.evalJSON(data);
					if ( !data.success ) {
						_gid = _gfalse(data.msg);
						_ghoverclose(_gid);
					}
					_gtrue(data.msg);
					_loadgrid1();
				},"text");
			});
		}
	});
};