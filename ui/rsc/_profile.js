if (typeof (_profileuid) !== 'undefined' && typeof (_profilegid) !== 'undefined') {
		$("button[name=btprofile], a[href^=#changepassword]").live("click", function (e) {
			e = e || window.event;
			e.preventDefault();
			var _id = _profileuid;
			$.ajaxSetup({ async: false, global: false, cache: true });
			var _pid = 'div#'+_profilegid, _tpl = 'profile', _formid = _profilegid+'user-profile';
			var _height = $(window).height() - 120;
			$(_pid).load(_index, { _req: 'tpl', _f: _tpl, id: _id, _formid: _formid }).dialog({
				modal: true,
				width: 600,
				resizable: true,
				position: ["center", "center"],
				buttons: {
					"Update": function () {
						$('#' + _formid).ajaxSubmit({
							url: _index,
							type: 'POST',
							method: 'POST',
							dataType: 'text',
							async: false,
							clearForm: false,
							resetForm: false,
							cache: false,
							data: { _post: 'profile', _what: 'user', id: _id },
							success: function (data) {
								if (!_ismsg_json(data)) {
									_gerror(data);
									return false;
								}
								data = $.evalJSON(data);
								if (!data.success) {
									_gid = _gfalse(data.msg);
									_ghoverclose(_gid);
									return false;
								}
								_gid = _gtrue(data.msg);
								_ghoverclose(_gid);
								window.setTimeout(function () {
									$(_pid).dialog("close");
									_gmsgremove();
								}, 1000);
							}
						});
						return false;
					},
					"Cancel": function () {
						$(this).dialog("close");
					}
				},
				open: function () {
					_dialog_maxheight($(this),_height);
					_buttondialog($(this));
				},
				close: function () {
					$(_pid).empty().dialog("destroy");
				}
			});
		});
};