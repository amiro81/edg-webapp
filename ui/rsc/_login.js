/** tpl: tpl/_login.cshtml **/

$(document).ready(function () {

	/* remove tabs cookie */
	function _delcookietab() {
		for(var x=0;x <= 20; x++) {
			$.delCookie("ui-tabs-"+x);
		}
		/*$.delCookie("cookietab");*/
		/*$.delCookie("masterid");*/
	};
	
	_delcookietab();
	
	function _clear() {
		$('input[name=uname], input[name=upass]').val('');
		$('input[name=uname]').focus();
	};

	$('input[name=uname], input[name=upass]').live("click", function (e) {
		e = e || window.event;
		e.preventDefault();
		$(this).val('');
	});

	$('form#login-form').live("submit", function () {
		$(this).ajaxSubmit({
			url: _index,
			type: 'POST',
			method: 'POST',
			dataType: 'text',
			async: false,
			clearForm: false,
			resetForm: false,
			cache: false,
			data: { _req: 'login' },
			beforeSubmit: function () {
				var _u, _p;
				_u = $('input[name=uname]').attr('value');
				_p = $.trim($('input[name=upass]').attr('value'));
				if (_u == '') {
					$('input[name=uname]').focus();
					return false;
				}
				if (_p == '') {
					$('input[name=upass]').focus();
					return false;
				}
			},
			success: function (data) {
				if (!_ismsg_json(data)) {
					_gerror(data);
					_clear();
					return false;
				}
				data = $.evalJSON(data);
				if (!data.success) {
					_gid = _gfalse(data.msg);
					_ghoverclose(_gid);
					_clear();
					return false;
				}
				_ajaxmsg(data.msg);
				_delcookietab();
				window.location.hash = '!p';
				window.setTimeout(_winreload, 500);
			}
		});
		return false;
	});

	$("button[name=btlogin]").live("click", function (e) {
		e = e || window.event;
		e.preventDefault();
		$('form#login-form').submit();
		return false;
	});
});
/** eol **/