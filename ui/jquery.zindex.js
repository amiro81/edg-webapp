/*
 * jQuery zindex plugin
 *
 * zindex - Calculates the highest CSS z-index value in
 *	    the current document or specified set of elements.
 *
 * Original source available: http://topzindex.googlecode.com/
 * Date: Fri 25 Jun 2010 04:32:36 PM MYT
 *
 * @author Mohd Nawawi Mohamad Jamili
 * @version 1
 */

(function($) {
	$.zindex = function(selector) {
		var _t = Math.max(0, Math.max.apply(null, $.map($(selector || "body *"),
			function(v) {
				return parseInt($(v).css("z-index")) || 1;
			})
		));
		return ( _t < 0 ) ? 1 : _t;
	};

	$.fn.zindex = function(opt) {
		if (this.length === 0) return this;
		opt = $.extend({increment: 1, selector: "body *"}, opt);
		var zmax = $.zindex(opt.selector), inc = opt.increment;
		return this.each(function () {
			$(this).css("z-index", zmax += inc);
		});
	};
})(jQuery);

