/*!
 * jQuery tooltip plugin
 *
 * xtooltip - Replace default tooltip by scanning data-tooltip element.
 * define #xtooltip css to replace default css
 *
 * Original source available: http://bassistance.de/jquery-plugins/jquery-plugin-tooltip/
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Version: 1
 * Author: Mohd Nawawi Mohd Jamili
 * Date: Thu 24 Jun 2010 05:47:40 PM MYT
 * Version: 1.1
 * Author: Mohd Nawawi Mohd Jamili
 * Date: Sat 06 Nov 2010 02:23:01 AM MYT
 */

;(function($) {
	var helper = {}, current, title, track = false, _timer = null;
	
	$.fn.xtooltip=function(settings) {
		settings=$.extend({
			delay: 300,
			top: 15,
			left: 15,
			wrap: 50
		},settings);
		createHelper(settings);
		return this.each(function () {
			$.data(this, "xtooltip", settings);
			var _text = $(this).attr('data-tooltip');
			if ( wordwrap instanceof Function ) {
				this.tooltipText = wordwrap(_text, settings.wrap, '<br>', true);
			} else {
				this.tooltipText = _text;
			}
			$(this).removeAttr("title").removeAttr("alt");
		}).mouseover(save).bind("mouseout click", hide);
	};

	function createHelper(settings) {
		if (helper.parent) {
			return;
		}
		helper.parent = $('<div id="xtooltip"></div>').appendTo('body').hide();
	};
	
	function settings(element) {
		return $.data(element, "xtooltip");
	};
	
	function handle(event) {
		_timer = setTimeout(function() { show(); }, settings(current).delay);
		track = !!settings(this).track;
		$(document.body).bind('mousemove', update);
		update(event);
	};
	
	function save() {
		if ( this == current || !this.tooltipText ) {
			return;
		}
		current = this;
		title = this.tooltipText;
		
		helper.parent.html(title);		
		handle.apply(this, arguments);
	};

	function show() {
		if (current == null) {
			return;
		}
		helper.parent.css('z-index','999999').show();
		update();
	};
	
	function update(event)	{
		var left, top, right, v, h;

		if (event && event.target.tagName == "OPTION") {
			return;
		}

		if ( !track && helper.parent.is(":visible")) {
			$(document.body).unbind('mousemove', update)
		}

		if (current == null) {
			$(document.body).unbind('mousemove', update);
			return;	
		}

		left = helper.parent[0].offsetLeft;
		top = helper.parent[0].offsetTop;
		if (event) {
			// position the helper 15 pixel to bottom right, starting from mouse position
			left = event.pageX + settings(current).left;
			top = event.pageY + settings(current).top;
			right='auto';
			if (settings(current).positionLeft) {
				right = $(window).width() - left;
				left = 'auto';
			}
			helper.parent.css({
				left: left,
				right: right,
				top: top
			});
		}
		
		v = viewport();
		h = helper.parent[0];

		/* check horizontal position */
		if (v.x + v.cx < h.offsetLeft + h.offsetWidth) {
			left -= h.offsetWidth + 20 + settings(current).left;
			helper.parent.css({left: left + 'px'});
		}

		/* check vertical position */
		if (v.y + v.cy < h.offsetTop + h.offsetHeight) {
			top -= h.offsetHeight + 20 + settings(current).top;
			helper.parent.css({top: top + 'px'});
		}
	};
	
	function viewport() {
		return {
			x: $(window).scrollLeft(),
			y: $(window).scrollTop(),
			cx: $(window).width(),
			cy: $(window).height()
		};
	};
	
	// hide helper and restore added classes and the title
	function hide() {
		current = null;
		if ( _timer != null ) {
			clearTimeout(_timer);
			_timer = null;
		}
		if ( helper.parent ) {
			helper.parent.hide();
		}
	};

	$(document).ready(function() {
		$(document.body).bind('mouseover.xtooltip',function() {
			$(this).find('[data-tooltip]').each(function() {
				$(this).xtooltip();
			});
		}).bind("mouseout click", function() {
			hide();
		}).bind("unload", function() {
			$(this).unbind("mouseover.xtooltip");
		});
	});
})(jQuery);
