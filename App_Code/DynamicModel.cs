using Massive;
using System.Text.RegularExpressions;

public class TblUsers : DynamicModel {
	public TblUsers():base(Ux.appconn, "Users", "UserId") {}
	public object getinfo(int UserId) {
		dynamic tbl = new TblUsers();
		object data = tbl.FindBy(UserId:UserId);
		return data;
	}
}

public class Tblfiles : DynamicModel {
	public Tblfiles():base(Ux.appconn, "Files", "Id") {}

	public object getinfo( int Id ) {
		dynamic tbl = new Tblfiles();
		object data = tbl.FindBy( Id: Id );
		return data;
	}
}

public class TblContent : DynamicModel {
	public TblContent() : base( Ux.appconn, "Content", "Id" ) { }

	public object getinfo( int Id ) {
		dynamic tbl = new TblContent();
		object data = tbl.FindBy( Id: Id );
		return data;
	}
}

public class TblDG : DynamicModel {
	public TblDG() : base( Ux.appconn, "Dangerous_Goods", "DGID" ) {}
}

public class TblPort : DynamicModel {
	public TblPort() : base( Ux.appconn, "Port", "PortId" ) {}
}

public class TblConsignee : DynamicModel {
	public TblConsignee() : base( Ux.appconn, "Consignee", "Id" ) { }
}

public class TblVessel : DynamicModel {
	public TblVessel() : base( Ux.appconn, "Vessel", "VesselId" ) { }
}

public class TblTypeOfVessel : DynamicModel {
	public TblTypeOfVessel() : base( Ux.appconn, "Type_Of_Vessel", "TypeOfVesselID" ) { }
}

public class TblCountry : DynamicModel {
	public TblCountry() : base( Ux.appconn, "Country", "Id" ) {}
}

public class TblAudittrail : DynamicModel {
	public TblAudittrail() : base( Ux.appconn, "Audittrail", "Id" ) { }
}

public class TblMembership : DynamicModel {
	public TblMembership() : base ( Ux.appconn, "webpages_Membership", "UserId") {}
}

public class TblDeclaration_Form_Master : DynamicModel {
	public TblDeclaration_Form_Master() : base( Ux.appconn, "Declaration_Form_Master", "Master_ID" ) { }
	public object getinfo( int Master_ID ) {
		dynamic tbl = new TblDeclaration_Form_Master();
		object data = tbl.FindBy( Master_ID: Master_ID );
		return data;
	}
	public void dodelete(int masterid) {
		dynamic xtbl0 = new TblDeclaration_Form_Master();
		dynamic xtbl1 = new TblDeclaration_Form_Detail();
		dynamic xtbl2 = new TblForm_Sub_Detail();
		dynamic xtbl3 = new TblMsg();
		xtbl0.Delete(masterid );

		var xx1 = xtbl1.FindBy( Master_ID: masterid );
		foreach ( var x1 in xx1 ) {
			xtbl1.Delete( x1.FormDetail_ID );
		}

		var xx2 = xtbl2.FindBy( Master_ID: masterid );
		foreach ( var x2 in xx2 ) {
			xtbl2.Delete( x2.IDSubDetail );
		}

		var xx3 = xtbl3.FindBy( Master_ID: masterid );
		foreach ( var x3 in xx3 ) {
			xtbl2.Delete( x3.Id );
		}

	}

}

public class TblDeclaration_Form_Detail : DynamicModel {
	public TblDeclaration_Form_Detail() : base( Ux.appconn, "Declaration_Form_Detail", "FormDetail_ID" ) { }
	public object getinfo( int FormDetail_ID ) {
		dynamic tbl = new TblDeclaration_Form_Detail();
		object data = tbl.FindBy( FormDetail_ID: FormDetail_ID );
		return data;
	}
	public void dodelete( int FormDetail_ID ) {
		dynamic xtbl1 = new TblDeclaration_Form_Detail();
		dynamic xtbl2 = new TblForm_Sub_Detail();
		dynamic xtbl3 = new TblMsg();

		xtbl1.Delete( FormDetail_ID );

		var xx2 = xtbl2.FindBy( FormDetail_ID: FormDetail_ID );
		foreach ( var x2 in xx2 ) {
			xtbl2.Delete( x2.IDSubDetail );
			xtbl3.Delete( x2.IDSubDetail );
		}

	}
}

public class TblForm_Sub_Detail : DynamicModel {
	public TblForm_Sub_Detail() : base( Ux.appconn, "Form_Sub_Detail", "IDSubDetail" ) { }
	public object getinfo( int IDSubDetail ) {
		dynamic tbl = new TblForm_Sub_Detail();
		object data = tbl.FindBy( IDSubDetail: IDSubDetail );
		return data;
	}
}

public class TblTradename : DynamicModel {
	public TblTradename() : base( Ux.appconn, "Tradename", "Id" ) { }
}

public class TblType_Of_Packing : DynamicModel {
	public TblType_Of_Packing() : base( Ux.appconn, "Type_Of_Packing", "TypeOfPackingID" ) { }
}

public class Db : DynamicModel {
	public Db() : base( Ux.appconn ) { }

	public object list_NameOfVessel() {
		dynamic tbl = new Db();
		object data = tbl.Query( "select DISTINCT NameOfVessel from Vessel order by NameOfVessel" );
		return data;
	}

	public object list_TypeOfVessel() {
		dynamic tbl = new Db();
		object data = tbl.Query( "select DISTINCT TypeOfVessel from Type_Of_Vessel order by TypeOfVessel" );
		return data;
	}

	public object list_Port() {
		dynamic tbl = new Db();
		object data = tbl.Query( "select Name,TimeLimit from Port order by Name" );
		return data;
	}

	public object list_Tradename() {
		dynamic tbl = new Db();
		object data = tbl.Query( "select Name from Tradename where Name!='' order by Name" );
		return data;
	}

}

public class TblMsg : DynamicModel {
	public TblMsg() : base ( Ux.appconn, "Msg", "Id") {}
	public object getinfo( int Id ) {
		dynamic tbl = new TblMsg();
		object data = tbl.FindBy( Id: Id );
		return data;
	}
	public void insert(object data) {
		dynamic tbl = new TblMsg();
		tbl.Insert(data);
	}
}

public class TblSMSLog : DynamicModel {
	public TblSMSLog() : base( Ux.appconn, "SMSLog", "Id" ) { }
}

public class TblSMSCredit : DynamicModel {
	public TblSMSCredit() : base( Ux.appconn, "SMSCredit", "Id" ) { }
}

public class TblSession : DynamicModel {
	public TblSession() : base(Ux.appconn, "ASPStateTempSessions", "SessionId") {}
}

public class TblUsers3 : DynamicModel {
	public TblUsers3():base(Ux.appconn, "Users3", "UserId") {}
}