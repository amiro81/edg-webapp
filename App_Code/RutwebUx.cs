using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Security;
using System.Web.WebPages;
using System.Security.Cryptography;
using System.Globalization;
using System.Dynamic;
using System.Collections.Generic;
using System.Web.SessionState;
using WebMatrix.WebData;
using Yahoo.Yui.Compressor;
using System.Data.SqlClient;
public class Ux {
	public static string appconn ="DGWEB";
	public static string vs = "09012013";
	public static string apptitle = "e-DG - Kuantan Port Dangerous Goods Declaration System";
	public static bool IsLogin = false;
	public static bool IsAdmin = false;
	public static bool IsKPA = false;
	public static bool IsKPC = false;
	public static bool IsAgent = false;
	public static bool IsSecurity = false;
	public static bool IsView = false;
	public static bool IS_AJAX = false;
	public static string UserLogin;
	public static string defaultpassword = "edg2012";
	public static int UserId;
	public static bool CacheReport = false;
	/*static Ux() {
		appconn = getconnname();
	}

	public static string getconnname() {
		if ( ConfigurationManager.ConnectionStrings.Count > 1 ) {
			return ConfigurationManager.ConnectionStrings[1].Name;
		}
		throw new InvalidOperationException( "Need a connection string name - can't determine what it is" );
	}*/

	public static void AppStart() {
		//HttpContext.Current.Application["xs"] = "";
		WebSecurity.InitializeDatabaseConnection( Ux.appconn, "Users", "UserId", "Login", true );
		string[] roles = new string[6] { "Admin", "KPA", "KPC", "Security", "Agent", "View" };
		foreach ( var role in roles ) {
			if ( !Roles.RoleExists( role ) ) {
				Roles.CreateRole( role );
			}
		}
		if ( !WebSecurity.UserExists( "sysadmin" ) ) {
			var udb = new TblUsers();
			udb.Insert( new { Login = "sysadmin", Name = "sysadmin", Enable = 1, Sms = 0 } );
			try {
				WebSecurity.CreateAccount( "sysadmin", "sysadmin", false );
			}
			catch ( System.Web.Security.MembershipCreateUserException e ) { throw new InvalidOperationException( e.ToString() ); }

			if ( !Roles.IsUserInRole( "sysadmin", "Admin" ) ) {
				try {
					Roles.AddUserToRole( "sysadmin", "Admin" );
				}
				catch ( System.Configuration.Provider.ProviderException e ) { throw new InvalidOperationException( e.ToString() ); }
			}
		}
		Ux.resetonline();
	}

	public static void PageStart() {
		Ux.setrole();
		Ux.IS_AJAX = ( HttpContext.Current.Request.Headers["X-Requested-With"] == "xmlhttprequest" ? true : false );
	}

	public static string xhost() {
		string port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
		if ( port == null || port == "80" || port == "443" ) {
			port = "";
		} else {
			port = ":" + port;
		}
		string protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
		if ( protocol == null || protocol == "0" ) {
			protocol = "http://";
		} else {
			protocol = "https://";
		}
		string sOut = protocol + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port;
		if ( sOut.EndsWith( "/" ) ) {
			sOut = sOut.Substring( 0, sOut.Length - 1 );
		}
		return sOut;
	}

	public static string basehost() {
		string sOut = Ux.xhost() + HttpContext.Current.Request.ApplicationPath;
		if ( sOut.EndsWith( "/" ) ) {
			sOut = sOut.Substring( 0, sOut.Length - 1 );
		}
		return sOut;
	}

	public static string urlhost() {
		string url = HttpContext.Current.Request.Url.AbsoluteUri;
		if ( url.EndsWith( "/" ) ) {
			url = url.Substring( 0, url.Length - 1 );
		}
		return url;
	}

	public static string href( string url ) {
		return Ux.basehost() + "/" + url + "?" + Ux.vs;
	}

	public static IHtmlString tohref( string src ) {
		src = new Regex( "href=\"(.*?)\"", RegexOptions.Compiled | RegexOptions.Singleline ).Replace( src, "href=\"" + Ux.xhost() + "$1\"" );
		src = new Regex( "src=\"(.*?)\"", RegexOptions.Compiled | RegexOptions.Singleline ).Replace( src, "src=\"" + Ux.xhost() + "$1\"" );
		return ( new HtmlString( src ) );
	}

	public static IHtmlString css( string file, bool header = true, bool compress = false ) {
		string css = null;
		var script = HttpContext.Current.Server.MapPath( file );
		if ( File.Exists( script ) ) {
			var streamReader = File.OpenText( script );
			var buff = streamReader.ReadToEnd();
			streamReader.Close();
			if ( buff != null ) {
				string minify = "";
				if ( compress ) {
					minify = CssCompressor.Compress( buff );
				} else {
					minify = buff;
				}
				if ( header ) {
					css = "<style type=\"text/css\">" + minify + "</style>";
				} else {
					css = minify;
				}
			}
		}
		return ( new HtmlString( css ) );
	}

	public static IHtmlString js( string file, bool header = true, bool compress = true ) {
		string js = null;
		var script = HttpContext.Current.Server.MapPath( file );
		if ( File.Exists( script ) ) {
			var streamReader = File.OpenText( script );
			var buff = streamReader.ReadToEnd();
			streamReader.Close();
			if ( buff != null ) {
				string minify = "";
				if ( compress ) {
					minify = JavaScriptCompressor.Compress( buff, true, true, false, false, -1, Encoding.UTF8, System.Globalization.CultureInfo.InvariantCulture );
				} else {
					minify = buff;
				}
				if ( header ) {
					js = "<script type=\"text/javascript\">" + minify + "</script>";
				} else {
					js = minify;
				}
			}
		}
		return ( new HtmlString( js ) );
	}

	public static IHtmlString include( string file ) {
		string ret = null;
		var script = HttpContext.Current.Server.MapPath( file );
		if ( File.Exists( script ) ) {
			var streamReader = File.OpenText( script );
			var buff = streamReader.ReadToEnd();
			streamReader.Close();
			if ( buff != null ) {
				ret = buff;
			}
		}
		return ( new HtmlString( ret ) );
	}

	public static void setrole() {
		Ux.IsLogin = false;
		Ux.IsAdmin = false;
		Ux.IsKPA = false;
		Ux.IsKPC = false;
		Ux.IsSecurity = false;
		Ux.IsAgent = false;
		Ux.IsView = false;
		if ( WebSecurity.IsAuthenticated && Ux.checkonline() ) {
			Ux.IsLogin = true;
			Ux.IsAdmin = ( Roles.IsUserInRole( "Admin" ) ? true : false );
			Ux.IsKPA = ( Ux.IsAdmin || Roles.IsUserInRole( "KPA" ) ? true : false );
			Ux.IsKPC = ( Ux.IsAdmin || Roles.IsUserInRole( "KPC" ) ? true : false );
			Ux.IsSecurity = ( Ux.IsAdmin || Roles.IsUserInRole( "Security" ) ? true : false );
			Ux.IsAgent = ( Roles.IsUserInRole( "Agent" ) ? true : false );
			Ux.IsView = ( Roles.IsUserInRole( "View" ) ? true : false );
			if ( Ux.UserId <= 0 ) {
				Ux.UserId = WebSecurity.CurrentUserId;
			}
			if ( Ux.UserLogin.IsNullOrEmpty() ) {
				Ux.UserLogin = WebSecurity.CurrentUserName;
			}
		}
	}

	public static int checklogin( string uname, string upass, bool urem ) {
		// not login more than 6 month
		Ux.disableuser();

		string ua = HttpContext.Current.Request.UserAgent;
		string ip = HttpContext.Current.Request.UserHostAddress;
		dynamic tbl = new TblUsers();
		var tbs = tbl.FindBy( UserId: WebSecurity.GetUserId( uname ) );
		foreach ( var xx in tbs ) {
			if ( !xx.Enable ) {
				return -1;
			}
		}
		if ( WebSecurity.Login( uname, upass, urem ) ) {
			Ux.UserLogin = uname;
			Ux.UserId = WebSecurity.GetUserId( Ux.UserLogin );
			//HttpContext.Current.Application["xs"] = Ux.UserId;
			Ux.setrole();
			var save = new { Lastlogin = DateTime.Now, Uagent = ua, Lastip = ip, Isonline = 1, SessId = HttpContext.Current.Session.SessionID };
			tbl.Update( save, Ux.UserId );
			Ux.audittrail( true, "session_login", "Login success" );
			return 1;
		}
		Ux.audittrail( false, "session_login", "Login failed" );
		return 0;
	}

	public static IHtmlString logout() {
		Ux.audittrail( true, "session_logout", "Logout" );
		Ux.setoffline(WebSecurity.CurrentUserId);
		WebSecurity.Logout();
		Ux.IsLogin = false;
		return Ux.json_return( true, "Logout" );
	}

	public static void setoffline(int userid) {
		dynamic tbl = new TblUsers();
		tbl.Update(new {Isonline = 0, SessId = "" }, userid);
	}

	public static bool checkonline() {
		dynamic tbl = new TblUsers();
		bool online = tbl.First(UserId:WebSecurity.CurrentUserId,Columns:"Isonline").Isonline;
		if ( !online ) {
			Ux.audittrail( true, "session_logout", "Logout" );
			Ux.setoffline( WebSecurity.CurrentUserId );
			WebSecurity.Logout();
			Ux.IsLogin = false;
			return false;
		}
		return true;
	}

	public static void resetonline() {
		dynamic tbl = new TblUsers();
		dynamic tbl2 = new TblSession();
		// handle by ms sql service agent
		tbl2.Execute("EXEC DeleteExpiredSessions");
		var data = tbl.Query( "select Users.UserId,Users.SessId,ASPStateTempSessions.SessionId from Users,ASPStateTempSessions where Users.SessId IS NOT NULL and Users.SessId != '' and Users.Isonline ='1' and Users.SessId!=ASPStateTempSessions.SessionId" );
		foreach(var dt in data) {
			tbl.Update( new { Isonline = 0, SessId = "" }, dt.UserId );
		}
	}

	public static void disableuser() {
		dynamic tbl = new TblUsers();
		var rd = DateTime.Today.AddMonths( -6 ).ToString( "yyyy-MM-dd" );
		var data = tbl.Query( "select UserId from Users where Lock='0' and Login!='sysadmin' and Lastlogin <='" + rd + "'" );
		foreach(var tt in data) {
			tbl.Update( new { Lock = 1, Enable = 0, Isonline = 0, SessId = ""}, tt.UserId);
		}
	}

	public static void unlockuser(int UserId) {
		dynamic tbl = new TblUsers();
		tbl.Update( new { Lock = 0 }, UserId);
	}

	public static void audittrail( bool status, string action, string text ) {
		dynamic tbl = new TblAudittrail();
		string ua = HttpContext.Current.Request.UserAgent;
		string ip = HttpContext.Current.Request.UserHostAddress;
		if ( Ux.UserId <= 0 ) {
			Ux.UserId = WebSecurity.CurrentUserId;
		}
		if ( Ux.UserLogin.IsNullOrEmpty() ) {
			Ux.UserLogin = WebSecurity.CurrentUserName;
		}
		if ( Ux.UserId > 0 && !Ux.UserLogin.IsNullOrEmpty() ) {
			string urole = Ux.listuserrole( Ux.UserLogin );
			var save = new { UserId = Ux.UserId, Login = Ux.UserLogin, Role = urole, Timestamp = DateTime.Now, Uagent = ua, IPAddress = ip, Action = action, Status = status, Message = text };
			tbl.Insert( save );
		}
	}

	public static bool deleteuser( int UserId ) {
		dynamic tbl = new TblUsers();
		var _login = tbl.First( UserId: UserId, columns: "Login" ).Login;
		foreach ( string _rx in Roles.GetRolesForUser( _login ) ) {
			Roles.RemoveUserFromRole( _login, _rx );
		}
		if ( Membership.DeleteUser( _login, true ) ) {
			var tblm = new TblMembership();
			tblm.Delete(UserId);
			return true;
		}
		return false;
	}

	public static bool userinrole( string login, string role ) {
		string[] rols = Roles.GetRolesForUser( login );
		if ( rols.Length > 0 ) {
			foreach ( string rol in rols ) {
				if ( rol == role ) {
					return true;
				}
			}
		}
		return false;
	}

	public static bool updaterole( string login, string role ) {
		bool ret = true;
		foreach ( string _rx in Roles.GetRolesForUser( login ) ) {
			Roles.RemoveUserFromRole( login, _rx );
		}

		try {
			Roles.AddUserToRole( login, role );
		}
		catch ( Exception e ) {
			ret = false;
		}

		return ret;
	}

	public static string listuserrole( string uname, string sep = " " ) {
		string ret = "";
		foreach ( string _rx in Roles.GetRolesForUser( uname ) ) {
			ret += _rx + sep;
		}
		return ret;
	}

	public static IHtmlString json_return( bool status, string msg, string id = null) {
		object data = new { success = status, msg = msg, id = id };
		return ( new HtmlString( Json.Encode( data ) ) );
	}

	public static string escape_query( string sql ) {
		return sql.Replace( "'", "''" );
	}

	static string md5( string input ) {
		MD5 md5Hash = MD5.Create();
		// Convert the input string to a byte array and compute the hash.
		byte[] data = md5Hash.ComputeHash( Encoding.UTF8.GetBytes( input ) );

		// Create a new Stringbuilder to collect the bytes
		// and create a string.
		StringBuilder sBuilder = new StringBuilder();

		// Loop through each byte of the hashed data 
		// and format each one as a hexadecimal string.
		for ( int i = 0; i < data.Length; i++ ) {
			sBuilder.Append( data[i].ToString( "x2" ) );
		}

		// Return the hexadecimal string.
		return sBuilder.ToString();
	}

	public static string ckey() {
		Guid g = Guid.NewGuid();
		string source = Convert.ToBase64String( g.ToByteArray() );
		return md5( DateTime.Now + source );
	}

	// from: http://stackoverflow.com/questions/5342375/c-sharp-regex-email-validation
	public static bool check_email( string s ) {
		if ( !string.IsNullOrEmpty( s ) ) {
			var regex = new Regex( @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" );
			return ( regex.IsMatch( s ) && !s.EndsWith( "." ) );
		}
		return false;
	}

	public static bool check_num( string str ) {
		Regex regex = new Regex( "^[0-9]+$" );
		if ( regex.IsMatch( str ) ) {
			return true;
		}
		return false;
	}

	public static bool check_decimal( string str ) {
		Regex regex = new Regex( "^[0-9.]+$" );
		if ( regex.IsMatch( str ) ) {
			return true;
		}
		return false;
	}

	public static IHtmlString empty_text( string text, string _replace = null ) {
		_replace = ( !string.IsNullOrEmpty( _replace ) ? _replace : "-none-" );
		if ( !string.IsNullOrEmpty( text ) ) {
			return ( new HtmlString( text ) );
		}

		return ( new HtmlString( _replace ) );
	}

	public static double ConvertBytesToKilobytes( long bytes ) {
		return bytes / 1024f;
	}

	public static double ConvertBytesToMegabytes( long bytes ) {
		return ( bytes / 1024f ) / 1024f;
	}

	public static double ConvertKilobytesToMegabytes( long kilobytes ) {
		return kilobytes / 1024f;
	}

	public static IHtmlString imolabel( string code) {
		if ( !code.IsNullOrEmpty() ) {
			var file = "label/"+code.Trim()+".png";
			var label = HttpContext.Current.Server.MapPath( file );
			if ( !File.Exists( label ) ) {
				file = "label/none.png";
				label = HttpContext.Current.Server.MapPath( file );
			}
			if ( File.Exists( label ) ) {
				return ( new HtmlString( "<img style='float:left;margin-top:5px;' src='" + Ux.basehost() + "/" + file + "'>"+code ) );
			}
		}
		return Ux.empty_text(code);
	}

	public static IHtmlString contenttype_name( string type ) {
		Dictionary<string, string> bb = new Dictionary<string, string>();
		bb.Add( "news", "News and Event" );
		bb.Add( "helpdesk", "Helpdesk" );
		if ( !type.IsNullOrEmpty() ) {
			foreach(var b in bb ) {
				if ( b.Key == type ) {
					return ( new HtmlString( b.Value) );
				}
			}
		}
		return Ux.empty_text( type );
	}

	public static IHtmlString documenttype_name( string type ) {
		Dictionary<string, string> bb = new Dictionary<string, string>();
		bb.Add( "cargo", "Cargo Information" );
		bb.Add( "bylaw", "By law" );
		bb.Add( "portcircular", "Port Circular" );
		if ( !type.IsNullOrEmpty() ) {
			foreach ( var b in bb ) {
				if ( b.Key == type ) {
					return ( new HtmlString( b.Value ) );
				}
			}
		}
		return Ux.empty_text( type );
	}

	public static IHtmlString contenttype_datetime( DateTime ntime ) {
		if ( !ntime.ToString().IsNullOrEmpty() ) {
			string dformat = "dd-MM-yyyy hh:mm tt";
			string dtime = ntime.ToString( dformat );
			return ( new HtmlString(dtime) );
		}
		return ( new HtmlString( "" ) );
	}

	public static IHtmlString _datetime( DateTime ntime ) {
		if ( !ntime.ToString().IsNullOrEmpty() ) {
			string dformat = "dd-MM-yyyy hh:mm tt";
			string dtime = ntime.ToString( dformat );
			return ( new HtmlString( dtime ) );
		}
		return ( new HtmlString( "" ) );
	}

	public static IHtmlString strip_tags(string text) {
		if ( !text.IsNullOrEmpty() ) {
			Regex regHtml = new System.Text.RegularExpressions.Regex( "<[^>]*>" );
			string s = regHtml.Replace(text,"");
			return ( new HtmlString(s) );
		}
		return ( new HtmlString("") );
	}

	public static int create_appid() {
		string dformat = "yyyyMM";
		DateTime dtime = DateTime.Now;
		string appid = dtime.ToString( dformat );
		dynamic tbl = new TblDeclaration_Form_Master();
		var data = tbl.Query( "select Application_ID from Declaration_Form_Master order by Application_ID ASC" );
		string lastid = "";
		foreach ( var xx in data ) {
			if ( xx.Application_ID > 0 ) {
				var x = xx.Application_ID.ToString().Substring(0,6);
				if ( x == appid ) {
					lastid = xx.Application_ID.ToString();
				}
			}
		}
		if ( lastid.IsEmpty() ) {
			lastid = appid + "0000";
		}
		int p = lastid.AsInt();
		p++;
		return p;
	}

	public static IHtmlString _datalist( int FormDetail_ID, string type ) {
		dynamic tbl = new TblForm_Sub_Detail();
		var data = tbl.FindBy(FormDetail_ID:FormDetail_ID);
		var _ret = "";
		int cal = 0;
		foreach ( var po in data ) {
			if ( type == "PSOnboard" ) {
				if ( po.PSOnboard != null ) {
					_ret += "<li>" + po.PSOnboard + "</li>";
				}
			} else if ( type == "Weight" ) {
				if ( po.Weight != null ) {
					_ret += "<li>" + po.Weight + "</li>";
				}
			} else if ( type == "CTU_id" ) {
				if ( po.CTU_id != null ) {
					_ret += "<li>" + po.CTU_id + "</li>";
				}
			} else if ( type == "Dimension" ) {
				if ( po.Dimension != null ) {
					_ret += "<li>" + po.Dimension + "</li>";
				}
			} else if ( type == "CTUTeus" ) {
				if ( po.CTUTeus != null ) {
					_ret += "<li>" + po.CTUTeus + "</li>";
				}
			} else if ( type == "Type" ) {
				if ( po.Type != null ) {
					_ret += "<li>" + po.Type + "</li>";
				}
			} else if ( type == "PSOnboard_Cargo" ) {
				if ( po.PSOnboard_Cargo != null ) {
					_ret += "<li>" + po.PSOnboard_Cargo + "</li>";
				}
			} else if ( type == "Lorry_ETA" ) {
				if ( po.Lorry_ETA != null ) {
					_ret += "<li>" + po.Lorry_ETA + "</li>";
				}
			} else if ( type == "Lorry_plate" ) {
				if ( po.Lorry_plate != null ) {
					_ret += "<li>" + po.Lorry_plate + "</li>";
				}
			} else if ( type == "Lorry_Company" ) {
				if ( po.Lorry_Company != null ) {
					_ret += "<li>" + po.Lorry_Company + "</li>";
				}
			}
			cal++;
		}
		if ( !_ret.IsEmpty() ) {
			if ( cal > 1 ) {
				_ret = "<ol style='list-style-type:lower-roman;margin:0px 0x 0x 5px;'>" + _ret + "</ol>";
			} else {
				_ret = "<ol style='list-style-type:none!important;margin:0px 0x 0x 5px;'>" + _ret + "</ol>";
			}
			return ( new HtmlString( _ret ) );
		}
		return Ux.empty_text( "" );
	}

	public static IHtmlString _weightcount( int FormDetail_ID) {
		dynamic tbl = new TblForm_Sub_Detail();
		var data = tbl.FindBy( FormDetail_ID: FormDetail_ID );
		int cal = 0;
		string pp = "";
		foreach ( var po in data ) {
			pp = po.Weight;
			cal += pp.AsInt();
		}
		return ( new HtmlString( cal.ToString() ) );
	}

	public static IHtmlString getuserlogin( string UserId ) {
		if ( !UserId.IsNullOrEmpty() ) {
			dynamic tbl = new TblUsers();
			string login = tbl.First( UserId: UserId.AsInt(), Columns: "Login" ).Login;
			if ( !login.IsEmpty() ) {
				return ( new HtmlString( login ) );
			}
		}
		return Ux.empty_text( "" );
	}

	public static bool _resetpassword( string UserId, string password ) {
		bool result = true;
		if ( !UserId.IsNullOrEmpty() ) {
			dynamic tbl = new TblUsers();
			string login = tbl.First( UserId: UserId.AsInt(), Columns: "Login" ).Login;
			if ( !login.IsEmpty()  ) {
				if ( login == "sysadmin" ) {
					return false;
				} else {
					string token = WebSecurity.GeneratePasswordResetToken( login );
					try {
						if ( !WebSecurity.ResetPassword( token, password ) ) {
							result = false;
						}
					}
					catch ( Exception e ) {
						result = false;
					}
				}
			}
		}
		return result;
	}

	public static IHtmlString _Submit_Date(DateTime dt) {
		string format = "dd/MM/yyyy hh:mm tt";
		return ( new HtmlString( dt.ToString( format ) ));
	}

	public static IHtmlString _ETA( DateTime dt, DateTime de ) {
		string tformat = "hh:mm tt";
		string dformat = "dd MMMM yyyy";
		return ( new HtmlString( dt.ToString( tformat )+"<br>"+de.ToString(dformat) ) );
	}

	public static void _clean_agent_form() {
		if ( Ux.IsAgent ) {
			int id = WebSecurity.CurrentUserId;
			dynamic tbl = new TblDeclaration_Form_Master();
			var data = tbl.FindBy(Agent_ID:id, Confirmation:0);
			foreach( var dd in data) {
				tbl.dodelete(dd.Master_ID);
			}
		}
	}

	public static string _removebr(string str) {
		str = str.Replace("%3Cbr%3E","");
		str = str.Replace("<br>","");
		return str; 
	}

	public static string _strcut(string  str,int len) {
		if ( str.Length > len ) {
			return str.Substring(0,len)+"..";
		}
		return str;
	}

	public static bool _nosmatch(string tname) {
		Match match = Regex.Match(tname,@"N\.O\.S");
		if (match.Success) {
			return true;
		}
		return false;
	}

	public static string _checkcredit_sms() {
		string url = "http://gateway.trio-mobile.com:81/bulkcheck.aspx";
		string smsid = "bulk60389275213lVtlr";
		string smspass = "bulk60389275213lVtlrlVtlr";

		WebRequest _Request = default( WebRequest );
		WebResponse _Response = default( WebResponse );
		string _credit = null;

		try {
			_Request = System.Net.WebRequest.Create( url );
			( (HttpWebRequest)_Request ).UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11";
			_Request.Headers.Add( "passname", smsid );
			_Request.Headers.Add( "password", smspass );
			_Response = _Request.GetResponse();
			_credit = _Response.Headers["credit"];
			_Response.Close();
		}
		catch ( Exception ex ) {
			_credit = ex.Message;
		}
		return _credit;
	}

	public static string _updatecredit() {
		string credit = Ux._checkcredit_sms();
		if ( !credit.IsEmpty() ) {
			dynamic tbl = new TblSMSCredit();
			var dt = tbl.All();
			foreach ( var x in dt ) {
				tbl.Delete( x.Id );
			}
			var save = new { Timestamp = DateTime.Now, Credit = credit };
			tbl.Insert( save );
			return credit;
		}
		return "0";
	}

	public static string _smscredit() {
		string credit = "-1";
		dynamic tbl = new TblSMSCredit();
		var dt = tbl.All();
		foreach ( var x in dt ) {
			credit = x.Credit;
		}
		if ( credit.IsEmpty() || credit == "-1") {
			credit = Ux._updatecredit();
		}
		return credit;
	}

	public static bool _dosendsms( string mobile, string text, string Application_ID ) {
		string url = "http://gateway.trio-mobile.com:81/cpm_i.aspx";
		string smsid = "bulk60389275213lVtlr";
		string smspass = "bulk60389275213lVtlrlVtlr";
		var _msg = "";
		bool _send = true;
		if ( !mobile.StartsWith( "6" ) ) {
			mobile = "6" + mobile;
		}

		text = "RM0.00 " + text;

		WebRequest _Request = default( WebRequest );
		WebResponse _Response = default( WebResponse );
		string _result = null;

		try {
			_Request = System.Net.WebRequest.Create( url );
			( (HttpWebRequest)_Request ).UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11";
			//_Request.Headers.Add( "user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11" );
			_Request.Headers.Add( "passname", smsid );
			_Request.Headers.Add( "password", smspass );
			_Request.Headers.Add( "trx_id", "0" );
			_Request.Headers.Add( "short_code", "36828" );
			_Request.Headers.Add( "originating_no", "dgweb" );
			_Request.Headers.Add( "destination_no", mobile );
			_Request.Headers.Add( "cp_ref_id", "0" );
			_Request.Headers.Add( "bill_type", "0" );
			_Request.Headers.Add( "bill_price", "0" );
			_Request.Headers.Add( "content_type", "1" );
			_Request.Headers.Add( "msg", text );
			_Request.Headers.Add( "bulk_fg", "1" );
			_Response = _Request.GetResponse();
			_result = _Response.Headers["result"];
			if ( _result.AsInt() > 0 ) {
				_send = true;
			}
			_Response.Close();
		}
		catch ( Exception ex ) {
			_msg = ex.Message;
			_send = false;
		}
		dynamic tbl = new TblSMSLog();
		var save = new { Timestamp = DateTime.Now, AppID = Application_ID, Mobile = mobile, Code = _result, Error = _msg, Status = _send, Text = text };
		tbl.Insert( save );
		return _send;
	}

	public static void sms_kpa( string Application_ID ) {
		string text = "Application ID: " + Application_ID + " is received. Please logon to KPA E-DG Online Declaration System to check for details.";
		dynamic tbl = new Db();
		var data = tbl.Query("select Login,Mobile from Users where Lock = '0' and Enable = '1' and Sms = '1'");
		var test = "";
		foreach(var dt in data) {
			if ( !String.IsNullOrEmpty( dt.Mobile ) && Ux.check_num( dt.Mobile ) && Ux.userinrole( dt.Login, "KPA" ) ) {
				if ( Ux._dosendsms( dt.Mobile, text, Application_ID ) ) {
					test += "KPA -> "+dt.Mobile+" -> "+text+"\n";
				}
			}
		}
		var tpfile = HttpContext.Current.Server.MapPath( "~/rw/report/SMS.txt" );
		File.WriteAllText( @tpfile, test );
		Ux._updatecredit();
	}

	public static void sms_kpc( string Application_ID ) {
		string text = "Application ID: "+Application_ID+" is received. Please logon to KPC E-DG Online Declaration System to check for details.";
		var test = "";
		dynamic tbl = new Db();
		var data = tbl.Query( "select Login,Mobile from Users where Lock = '0' and Enable = '1' and Sms = '1'" );
		foreach ( var dt in data ) {
			if ( !String.IsNullOrEmpty( dt.Mobile ) && Ux.check_num( dt.Mobile ) && Ux.userinrole( dt.Login, "KPC" ) ) {
				if ( Ux._dosendsms( dt.Mobile, text, Application_ID ) ) {
					test += "KPC -> "+dt.Mobile+" -> "+text+"\n";
				}
			}
		}
		var tpfile = HttpContext.Current.Server.MapPath( "~/rw/report/SMS.txt" );
		File.WriteAllText( @tpfile, test );
		Ux._updatecredit();
	}

	public static void sms_pending( string Application_ID ) {
		string text = "Application ID: " + Application_ID + " is updated. Please logon to E-DG Online Declaration System to check for details.";
		var test = "";

		dynamic tbl = new Db();
		int? sid = null;
		var data1 = tbl.Query( string.Format( "select KPC_Approved_ID,KPA_Approved_ID from Declaration_Form_Master where Application_ID='{0}'", Application_ID ) );
		foreach(var xx in data1) {
			sid = xx.KPA_Approved_ID;
			if ( sid == null ) {
				sid = xx.KPC_Approved_ID;
			}
		}

		if ( sid != null ) {
			var data = tbl.Query( string.Format("select Login,Mobile from Users where UserId='{0}' and Lock = '0' and Enable = '1' and Sms = '1'", sid ) );
			foreach ( var dt in data ) {
				if ( !String.IsNullOrEmpty( dt.Mobile ) && Ux.check_num( dt.Mobile ) ) {
					if ( Ux._dosendsms( dt.Mobile, text, Application_ID ) ) {
						test += dt.Login+" -> " + dt.Mobile + " -> " + text + "\n";
					}
				}
			}
			var tpfile = HttpContext.Current.Server.MapPath( "~/rw/report/SMS.txt" );
			File.WriteAllText( @tpfile, test );
			Ux._updatecredit();
		}
	}

	public static IHtmlString _msgisread( string Application_ID ) {
		int cnt = 0;
		dynamic tbl = new TblMsg();
		var data = tbl.FindBy( Application_ID: Application_ID );
		foreach ( var dd in data ) {
			if ( Ux.IsAgent ) {
				if ( !dd.isread ) {
					cnt++;
				}
			} else {
				if ( !dd.isuread ) {
					cnt++;
				}
			}
		}
		return ( new HtmlString( cnt.ToString() ) );
	}

	public static void _setmsgread(int Id) {
		dynamic tbl = new TblMsg();
		double appid = tbl.First( Id: Id, Columns: "Application_ID" ).Application_ID;
		var data = tbl.FindBy( Application_ID: appid );
		foreach ( var dd in data ) {
			if ( Ux.IsAgent ) {
				if ( !dd.isread ) {
					tbl.Update( new { isread = 1 }, dd.Id );
				}
			} else {
				if ( !dd.isuread ) {
					tbl.Update( new { isuread = 1 }, dd.Id );
				}		
			}
		}
	}

	public static IHtmlString _msgsender(int UserId) {
		dynamic tbl = new TblUsers();
		string sender = tbl.First( UserId: UserId, Columns: "Login" ).Login;
		if ( !sender.IsEmpty() ) {
			if ( Ux.userinrole(sender,"KPC" )) {
				return ( new HtmlString( "KPC" ) );
			}
			if ( Ux.userinrole(sender,"KPA" )) {
				return ( new HtmlString( "KPA" ) );
			}

			return ( new HtmlString( sender ) );
		}
		return ( new HtmlString( "none" ) );
	}

	public static IHtmlString _vessel_company(string name) {
		var company = "-none-";
		if ( !name.IsNullOrEmpty() ) {
			dynamic tbl = new TblVessel();
			try {
				company = tbl.First( NameOfVessel: name, Columns: "vessel_company" ).vessel_company;
			} catch(Exception e) {
				company = "-none-";
			}
		}
		return ( new HtmlString( company ) );
	}

	public static void cleanup_unfinish_application() {
		dynamic tbl = new TblDeclaration_Form_Master();
		var rd = DateTime.Today.AddDays( -2 ).ToString( "yyyy-MM-dd" );
		var data = tbl.Query( "SELECT Declaration_Form_Master.* FROM Declaration_Form_Master,Users where Declaration_Form_Master.Agent_ID = Users.UserId and Declaration_Form_Master.Confirmation ='0' and Declaration_Form_Master.Submit_Date <='" + rd + "'" );
		foreach(var xx in data) {
			try {
				tbl.dodelete(xx.Master_ID);
			} catch(Exception e) {}
		}
	}

	public static void cleanup_noowner_application() {
		bool find = true;
		dynamic tbx = new TblDeclaration_Form_Master();
		dynamic tbl = new Db();
		var q = tbx.Query( "select /*Application_ID,*/Master_ID,Agent_ID,Name_of_Vessel from Declaration_Form_Master" );
		var q1 = tbl.Query( "select UserId from Users" );
		foreach ( var x in q ) {
			find = false;
			foreach(var xx in q1) {
				if ( x.Agent_ID == xx.UserId ) {
					find = true;
				}
			}
			if ( !find ) {
				try {
					tbx.dodelete(x.Master_ID);
				} catch ( Exception e ) { }
			}
		}

		q1 = tbl.Query( "select NameOfVessel from Vessel" );
		foreach ( var x in q ) {
			find = false;
			foreach ( var xx in q1 ) {
				if ( x.Name_of_Vessel == xx.NameOfVessel ) {
					find = true;
				}
			}
			if ( !find ) {
				try {
					tbx.dodelete( x.Master_ID );
				}
				catch ( Exception e ) { }
			}
		}
	}
	/*public static IHtmlString countuseronline() {
		dynamic tbl = new TblUsers();
		
	}*/
}