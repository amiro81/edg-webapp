﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace RutwebWs {

	public class ServerHeader : IHttpModule {

		public void Init( HttpApplication context ) {
			context.PreSendRequestHeaders += OnPreSendRequestHeaders;
		}

		public void Dispose() { }

		private void OnPreSendRequestHeaders( object sender, EventArgs e ) {
			// modify the "Server" Http Header
			HttpContext.Current.Response.Headers.Set( "Server", "Rutweb/Ws" );
		}
	}

	public enum CompressOptions {
		GZip,
		Deflate,
		None
	}
	
	public class WhitespaceFilter : Stream {
		private GZipStream _contentGZip;
		private DeflateStream _content_Deflate;
		private Stream _content;
		private CompressOptions _options;
		private string _contentType;

		public WhitespaceFilter( Stream content, CompressOptions options, string contentType ) {
			if ( options == CompressOptions.GZip ) {
				this._contentGZip = new GZipStream( content, CompressionMode.Compress );
				this._content = this._contentGZip;
			} else if ( options == CompressOptions.Deflate ) {
				this._content_Deflate = new DeflateStream( content, CompressionMode.Compress );
				this._content = this._content_Deflate;
			} else {
				this._content = content;
			}
			this._options = options;
			this._contentType = contentType;
		}


		public override bool CanRead {
			get { return this._content.CanRead; }
		}

		public override bool CanSeek {
			get { return this._content.CanSeek; }
		}

		public override bool CanWrite {
			get { return this._content.CanWrite; }
		}

		public override void Flush() {
			this._content.Flush();
		}

		public override long Length {
			get { return this._content.Length; }
		}

		public override long Position {
			get {
				return this._content.Position;
			}
			set {
				this._content.Position = value;
			}
		}

		public override int Read( byte[] buffer, int offset, int count ) {
			return this._content.Read( buffer, offset, count );
		}

		public override long Seek( long offset, SeekOrigin origin ) {
			return this._content.Seek( offset, origin );
		}

		public override void SetLength( long value ) {
			this._content.SetLength( value );
		}


		private string minify(string source) {
			string pat = "";
			pat += @"(<!DOCTYPE[^<>]*>)|(<head[^<>]*>)|(<title[^<>]*>[^<>]*</title>)|(<meta[^<>]*>)|(<script[^<>]*>)|(<link[^<>]*>)";
			pat += @"|(<pre[^<>]*>[^<>]*(((?<Open><)[^<>]*)+((?<Close-Open>>)[^<>]*)+)*(?(Open)(?!))</pre>)";
			pat += @"|(<textarea[^<>]*>[^<>]*(((?<Open><)[^<>]*)+((?<Close-Open>>)[^<>]*)+)*(?(Open)(?!))</textarea>)";
			pat += @"|(?<=[^])\t{2,}|(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,11}(?=[<])|(?=[\n])\s{2,}|\<\!--[^\[].*?--\>";
			source = new Regex( pat, RegexOptions.Compiled | RegexOptions.Multiline ).Replace( source, delegate(Match m) {
				if ( m.Value.Contains( "<!DOCTYPE" )
					|| m.Value.Contains( "<head" )
					|| m.Value.Contains( "<meta" )
					|| m.Value.Contains( "<title" )
					|| m.Value.Contains( "<script" )
					|| m.Value.Contains( "<link" )
				) {
					return m.Value + "\n";
				}
				if ( m.Value.Contains("<textarea") || m.Value.Contains("<pre") ) {
					return m.Value;
				}
				return "";
			});

			return source;
		}
		
		public override void Write( byte[] buffer, int offset, int count ) {
			byte[] data = new byte[count + 1];
			Buffer.BlockCopy( buffer, offset, data, 0, count );
			string source = System.Text.Encoding.UTF8.GetString( data );
			/*if ( this._contentType.Contains("html") ) {
				source = this.minify(source);
			}*/
			byte[] outdata = System.Text.Encoding.UTF8.GetBytes( source );
			this._content.Write( outdata, 0, outdata.GetLength( 0 ) );
		}

	}

	public class Compress : System.Web.HttpApplication {

		public Compress() {
			InitializeComponent();
		}

		private void InitializeComponent() {
			this.PostReleaseRequestState += new EventHandler( Compress_PostReleaseRequestState );
			//this.PreRequestHandlerExecute += new EventHandler( Global_PostReleaseRequestState );
		}

		private void Compress_PostReleaseRequestState(
			object sender, EventArgs e ) {
			string contentType = Response.ContentType;

			if ( contentType.Contains("html")
				/*|| contentType.Contains("css")
				|| contentType.Contains("javascript")*/ ) {

				string acceptEncoding = Request.Headers["Accept-Encoding"];
				Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
				if ( !string.IsNullOrEmpty( acceptEncoding ) ) {
					acceptEncoding = acceptEncoding.ToUpperInvariant();
					if ( acceptEncoding.Contains( "GZIP" ) ) {
						Response.Filter = new WhitespaceFilter( Response.Filter, CompressOptions.GZip, contentType );
						Response.AppendHeader( "Content-encoding", "gzip" );
					} else if ( acceptEncoding.Contains( "DEFLATE" ) ) {
						Response.Filter = new WhitespaceFilter( Response.Filter, CompressOptions.Deflate, contentType );
						Response.AppendHeader( "Content-encoding", "deflate" );
					} else {
						Response.Filter = new WhitespaceFilter( Response.Filter, CompressOptions.None, contentType );
					}
				} else {
					Response.Filter = new WhitespaceFilter( Response.Filter, CompressOptions.None, contentType );
				}
			}
		}
	}
}